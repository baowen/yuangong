// SQL SERVER

CREATE DATABASE EmployeeIMS    -- 创建数据库
GO
-- 以下为创建各表的SQL命令
CREATE TABLE [dbo].[DepartmentInformation](
[D_Number] [int] IDENTITY(1,1) NOT NULL,
[D_Name] [varchar](20) NOT NULL,
[D_Count] [int] NOT NULL, CONSTRAINT [PK_DepartmentInformation] PRIMARY KEY CLUSTERED ([D_Number] ASC ))
insert into DepartmentInformation values('市场部',10);
insert into DepartmentInformation values('生产部',200);
insert into DepartmentInformation values('财务部',5);
insert into DepartmentInformation values('培训部',4);
insert into DepartmentInformation values('后勤部',1);
insert into DepartmentInformation values('策划部',3);
insert into DepartmentInformation values('销售部',15);
insert into DepartmentInformation values('食堂',10);
insert into DepartmentInformation values('医院',30);

CREATE TABLE [dbo].[EmployeeInformation](
[E_Number] [int] IDENTITY(1,1) NOT NULL,
[E_Name] [varchar](30) NOT NULL,
[E_Sex] [varchar](2) NOT NULL,
[E_BornDate] [smalldatetime] NOT NULL,
[E_Marriage] [varchar](4) NOT NULL,
[E_PoliticsVisage] [varchar](20) NOT NULL,
[E_SchoolAge] [varchar](20) NULL,
[E_EnterDate] [smalldatetime] NULL,
[E_InDueFormDate] [smalldatetime] NOT NULL,
[D_Number] [int] NOT NULL,
[E_Headship] [varchar](20) NOT NULL,
[E_Estate] [varchar](10) NOT NULL,
[E_Remark] [varchar](500) NULL, CONSTRAINT [PK_EmployeeInformation] PRIMARY KEY CLUSTERED ([E_Number] ASC ))
insert into EmployeeInformation values('张勇','男','1983-5-17','未婚','党员','本科','2002-8-17','2008-8-17',1,'厂长','在职','');
insert into EmployeeInformation values('钱力','男','1996-1-2','未婚','党员','本科','2004-9-10','2006-6-5',2,'厂长','在职','高级工程师');

CREATE TABLE [dbo].[TrainInformation](
[ID] [int] IDENTITY(1,1) NOT NULL,
[T_Number] [varchar](20) NOT NULL,
[T_Content] [varchar](100) NOT NULL,
[E_Number] [int] NOT NULL,
[T_Date] [int] NULL,
[T_Money] [int] NULL, CONSTRAINT [PK_TrainInformation] PRIMARY KEY CLUSTERED([ID] ASC ))
insert into TrainInformation values('200701','职业素质',2,30,7000);
insert into TrainInformation values('200702','职业素质',1,31,7000);

CREATE TABLE [dbo].[WageInformation](
[ID] [int] IDENTITY(1,1) NOT NULL,
[W_Number] [int] NOT NULL,
[E_Number] [int] NOT NULL,
[W_BasicWage] [decimal](18, 2) NOT NULL,
[W_Boon] [decimal](18, 2) NOT NULL,
[W_Bonus] [decimal](18, 2) NOT NULL,
[W_FactWage] [decimal](18, 2) NOT NULL, CONSTRAINT [PK_WageInformation] PRIMARY KEY CLUSTERED ([ID] ASC ))
insert into WageInformation values(2,2,2111.00,2300.00,1100.00,5511.00);
insert into WageInformation values(3,1,3000.00,2300.00,1200.00,6500.00);

CREATE TABLE [dbo].[RewardspunishmentInformation](
[ID] [int] IDENTITY(1,1) NOT NULL,
[R_Number] [int] NOT NULL,
[E_Number] [int] NOT NULL,
[R_Date] [datetime] NOT NULL,
[R_Address] [varchar](50) NOT NULL,
[R_Causation] [varchar](200) NOT NULL,
[R_Remark] [varchar](500) NULL, CONSTRAINT [PK_EncouragementPunishInformation] PRIMARY KEY CLUSTERED ([ID] ASC ))
insert into RewardspunishmentInformation values(1,1,'2005-5-3','教学馆二楼','演讲比赛一等奖','');	  
insert into RewardspunishmentInformation values(2,2,'2004-5-3','教学馆二楼','演讲比赛一等奖','');	

CREATE TABLE [dbo].[UserInformation](
[User_ID] [int] IDENTITY(1,1) NOT NULL,
[User_Name] [varchar](20) NOT NULL,
[Password] [varchar](20) NOT NULL,
[Authority] [varchar](20) NULL DEFAULT ('B'),CONSTRAINT [PK_UserInformation] PRIMARY KEY CLUSTERED ([User_ID] ASC ))
insert into UserInformation values('管理员','1','A');
insert into UserInformation values('admin','123456','B');
insert into UserInformation values('GM','11','B');
-- 
ALTER TABLE [dbo].[EmployeeInformation]  WITH CHECK ADD  CONSTRAINT [FK_EmployeeInformation_DepartmentInformation] FOREIGN KEY([D_Number]) REFERENCES [dbo].[DepartmentInformation] ([D_Number])
ALTER TABLE [dbo].[TrainInformation]  WITH CHECK ADD  CONSTRAINT [FK_TrainInformation_EmployeeInformation] FOREIGN KEY([E_Number]) REFERENCES [dbo].[EmployeeInformation] ([E_Number])
ALTER TABLE [dbo].[WageInformation]  WITH CHECK ADD  CONSTRAINT [FK_WageInformation_EmployeeInformation] FOREIGN KEY([E_Number]) REFERENCES [dbo].[EmployeeInformation] ([E_Number])
ALTER TABLE [dbo].[RewardspunishmentInformation]  WITH CHECK ADD  CONSTRAINT [FK_EncouragementPunishInformation_EmployeeInformation] FOREIGN KEY([E_Number]) REFERENCES [dbo].[EmployeeInformation] ([E_Number])




// Mysql

drop table UserInformation;
CREATE TABLE UserInformation(User_ID int NOT NULL auto_increment,User_Name varchar(20) NOT NULL,Password varchar(20) NOT NULL,Authority  varchar(20) NULL default 'B',CONSTRAINT PK_UserInformation PRIMARY KEY(User_ID));
//alter table UserInformation modify Authority default 'B';
insert into UserInformation values(null,'管理员','1','A');
insert into UserInformation values(null,'admin','123456','B');
insert into UserInformation values(null,'GM','11','B');

drop table DepartmentInformation;
CREATE TABLE DepartmentInformation(D_Number int NOT NULL auto_increment,
D_Name varchar2(20) NOT NULL,
D_Count int NOT NULL, CONSTRAINT PK_DepartmentInformation PRIMARY KEY(D_Number));
insert into DepartmentInformation values(null,'市场部',10);
insert into DepartmentInformation values(null,'生产部',200);
insert into DepartmentInformation values(null,'财务部',5);
insert into DepartmentInformation values(null,'培训部',4);
insert into DepartmentInformation values(null,'后勤部',1);
insert into DepartmentInformation values(null,'策划部',3);
insert into DepartmentInformation values(null,'销售部',15);
insert into DepartmentInformation values(null,'食堂',10);
insert into DepartmentInformation values(null,'医院',30);

drop table EmployeeInformation;
CREATE TABLE EmployeeInformation(
E_Number int NOT NULL auto_increment,
E_Name varchar(30) NOT NULL,
E_Sex varchar(2) NOT NULL,
E_BornDate date NOT NULL,
E_Marriage varchar(4) NOT NULL,
E_PoliticsVisage varchar(20) NOT NULL,
E_SchoolAge varchar(20) NULL,
E_EnterDate date NULL,
E_InDueFormDate date NOT NULL,
D_Number int NOT NULL,
E_Headship varchar(20) NOT NULL,
E_Estate varchar(10) NOT NULL,
E_Remark varchar(500) NULL, CONSTRAINT PK_EmployeeInformation PRIMARY KEY(E_Number));
insert into EmployeeInformation values(null,'张勇','男','1983-5-17','未婚','党员','本科','2002-8-17','2008-8-17',1,'厂长','在职','');
insert into EmployeeInformation values(null,'钱力','男','1996-1-2','未婚','党员','本科','2004-9-10','2006-6-5',2,'厂长','在职','高级工程师');

CREATE TABLE TrainInformation(
ID int NOT NULL auto_increment,
T_Number varchar(20) NOT NULL,
T_Content varchar(100) NOT NULL,
E_Number int NOT NULL,
T_Date int NULL,
T_Money int NULL, CONSTRAINT PK_TrainInformation PRIMARY KEY(ID));
insert into TrainInformation values(null,'200701','职业素质',2,30,7000);
insert into TrainInformation values(null,'200702','职业素质',1,31,7000);

CREATE TABLE WageInformation(
ID int NOT NULL auto_increment,
W_Number int NOT NULL,
E_Number int NOT NULL,
W_BasicWage decimal(18, 2) NOT NULL,
W_Boon decimal(18, 2) NOT NULL,
W_Bonus decimal(18, 2) NOT NULL,
W_FactWage decimal(18, 2) NOT NULL, CONSTRAINT PK_WageInformation PRIMARY KEY(ID));
insert into WageInformation values(null,2,2,2111.00,2300.00,1100.00,5511.00);
insert into WageInformation values(null,3,1,3000.00,2300.00,1200.00,6500.00);

CREATE TABLE RewardspunishmentInformation(
ID int NOT NULL auto_increment,
R_Number int NOT NULL,
E_Number int NOT NULL,
R_Date date NOT NULL,
R_Address varchar(50) NOT NULL,
R_Causation varchar(200) NOT NULL,
R_Remark varchar(500) NULL, CONSTRAINT PK_EncouragementPunishInf PRIMARY KEY (ID));
insert into RewardspunishmentInformation values(null,1,1,'2005-5-3','教学馆二楼','演讲比赛一等奖','');	  
insert into RewardspunishmentInformation values(null,2,2,'2004-5-3','教学馆二楼','演讲比赛一等奖','');	 

ALTER TABLE EmployeeInformation ADD CONSTRAINT FK_EmployeeInf_DepartmentInf FOREIGN KEY(D_Number) 
REFERENCES DepartmentInformation(D_Number);

ALTER TABLE TrainInformation ADD CONSTRAINT FK_TrainInf_EmployeeInf FOREIGN KEY(E_Number) REFERENCES EmployeeInformation(E_Number);

ALTER TABLE WageInformation ADD CONSTRAINT FK_WageInf_EmployeeInf FOREIGN KEY(E_Number) REFERENCES EmployeeInformation(E_Number);

ALTER TABLE RewardspunishmentInformation ADD CONSTRAINT FK_EncouragePInf_EmployeeInf 
FOREIGN KEY(E_Number) REFERENCES EmployeeInformation(E_Number);




// Oracle

drop table UserInformation;
create sequence SQID minvalue 1 maxvalue 999999999999999999999999999 start with 1 increment by 1;
CREATE TABLE SCOTT.UserInformation(User_ID int NOT NULL,User_Name varchar2(20) NOT NULL,Password varchar2(20) NOT NULL,Authority  varchar2(20) NULL ,
 CONSTRAINT PK_UserInformation PRIMARY KEY(User_ID));
alter table SCOTT.UserInformation modify Authority default 'B';
insert into SCOTT.UserInformation values(SQID.nextval,'管理员','1','A');
insert into SCOTT.UserInformation values(SQID.nextval,'admin','123456','B');
insert into SCOTT.UserInformation values(SQID.nextval,'GM','11','B');

create sequence SDI minvalue 1 maxvalue 999999999999999999999999999 start with 1 increment by 1;
CREATE TABLE DepartmentInformation(
D_Number int NOT NULL,
D_Name varchar2(20) NOT NULL,
D_Count int NOT NULL, CONSTRAINT PK_DepartmentInformation PRIMARY KEY(D_Number));
insert into DepartmentInformation values(SDI.nextval,'市场部',10);
insert into DepartmentInformation values(SDI.nextval,'生产部',200);
insert into DepartmentInformation values(SDI.nextval,'财务部',5);
insert into DepartmentInformation values(SDI.nextval,'培训部',4);
insert into DepartmentInformation values(SDI.nextval,'后勤部',1);
insert into DepartmentInformation values(SDI.nextval,'策划部',3);
insert into DepartmentInformation values(SDI.nextval,'销售部',15);
insert into DepartmentInformation values(SDI.nextval,'食堂',10);
insert into DepartmentInformation values(SDI.nextval,'医院',30);

create sequence SEI minvalue 1 maxvalue 999999999999999999999999999 start with 1 increment by 1;
CREATE TABLE EmployeeInformation(
E_Number int NOT NULL,
E_Name varchar2(30) NOT NULL,
E_Sex varchar2(2) NOT NULL,
E_BornDate date NOT NULL,
E_Marriage varchar2(4) NOT NULL,
E_PoliticsVisage varchar2(20) NOT NULL,
E_SchoolAge varchar2(20) NULL,
E_EnterDate date NULL,
E_InDueFormDate date NOT NULL,
D_Number int NOT NULL,
E_Headship varchar2(20) NOT NULL,
E_Estate varchar2(10) NOT NULL,
E_Remark varchar2(500) NULL, CONSTRAINT PK_EmployeeInformation PRIMARY KEY(E_Number));
insert into EmployeeInformation values(SEI.nextval,'张勇','男',to_date('1983-5-17','YYYY-MM-DD'),'未婚','党员','本科',to_date('2002-8-17','YYYY-MM-DD'),to_date('2008-8-17','YYYY-MM-DD'),1,'厂长','在职','');
insert into EmployeeInformation values(SEI.nextval,'钱力','男',to_date('1996-1-2','YYYY-MM-DD'),'未婚','党员','本科',to_date('2004-9-10','YYYY-MM-DD'),to_date('2006-6-5','YYYY-MM-DD'),2,'厂长','在职','高级工程师');

create sequence STI minvalue 1 maxvalue 999999999999999999999999999 start with 1 increment by 1;
CREATE TABLE TrainInformation(
ID int NOT NULL,
T_Number varchar2(20) NOT NULL,
T_Content varchar2(100) NOT NULL,
E_Number int NOT NULL,
T_Date int NULL,
T_Money int NULL, CONSTRAINT PK_TrainInformation PRIMARY KEY(ID));
insert into TrainInformation values(STI.nextval,'200701','职业素质',2,30,7000);
insert into TrainInformation values(STI.nextval,'200702','职业素质',1,31,7000);

create sequence SWI minvalue 1 maxvalue 999999999999999999999999999 start with 1 increment by 1;
CREATE TABLE WageInformation(
ID int NOT NULL,
W_Number int NOT NULL,
E_Number int NOT NULL,
W_BasicWage decimal(18, 2) NOT NULL,
W_Boon decimal(18, 2) NOT NULL,
W_Bonus decimal(18, 2) NOT NULL,
W_FactWage decimal(18, 2) NOT NULL, CONSTRAINT PK_WageInformation PRIMARY KEY(ID));
insert into WageInformation values(SWI.nextval,2,2,2111.00,2300.00,1100.00,5511.00);
insert into WageInformation values(SWI.nextval,3,1,3000.00,2300.00,1200.00,6500.00);

create sequence SRI minvalue 1 maxvalue 999999999999999999999999999 start with 1 increment by 1;
CREATE TABLE RewardspunishmentInformation(
ID int NOT NULL,
R_Number int NOT NULL,
E_Number int NOT NULL,
R_Date date NOT NULL,
R_Address varchar2(50) NOT NULL,
R_Causation varchar2(200) NOT NULL,
R_Remark varchar2(500) NULL, CONSTRAINT PK_EncouragementPunishInf PRIMARY KEY (ID));
insert into RewardspunishmentInformation values(SRI.nextval,1,1,to_date('2005-5-3','YYYY-MM-DD'),'教学馆二楼','演讲比赛一等奖','');	  
insert into RewardspunishmentInformation values(SRI.nextval,2,2,to_date('2004-5-3','YYYY-MM-DD'),'教学馆二楼','演讲比赛一等奖','');	 

ALTER TABLE EmployeeInformation ADD CONSTRAINT FK_EmployeeInf_DepartmentInf FOREIGN KEY(D_Number) 
REFERENCES DepartmentInformation(D_Number);

ALTER TABLE TrainInformation ADD CONSTRAINT FK_TrainInf_EmployeeInf FOREIGN KEY(E_Number) REFERENCES EmployeeInformation(E_Number);

ALTER TABLE WageInformation ADD CONSTRAINT FK_WageInf_EmployeeInf FOREIGN KEY(E_Number) REFERENCES EmployeeInformation(E_Number);

ALTER TABLE RewardspunishmentInformation ADD CONSTRAINT FK_EncouragePInf_EmployeeInf 
FOREIGN KEY(E_Number) REFERENCES EmployeeInformation(E_Number);



// PostgreSQL

// 创建数据库并支持汉字
CREATE DATABASE "EmployeeIMS"
  WITH OWNER = qxz
       ENCODING = 'UNICODE'
       TABLESPACE = pg_default;

//CREATE SEQUENCE "SQID" INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 99999999;
//ALTER TABLE "SQID" OWNER TO qxz;
//CREATE TABLE UserInformation ( User_ID int NOT NULL,User_Name varchar(20) NOT NULL,Password varchar(20) //NOT NULL,Authority  varchar(20) NULL , CONSTRAINT PK_UserInformation PRIMARY KEY(User_ID));
drop table UserInformation;
CREATE SEQUENCE UserInformation_user_id_seq;
CREATE TABLE UserInformation (
   User_ID int4 NOT NULL DEFAULT nextval('UserInformation_user_id_seq'),
   User_Name varchar(20) NOT NULL,
   Password varchar(20) NOT NULL,
   Authority  varchar(20) NULL , 
   CONSTRAINT PK_UserInformation PRIMARY KEY(User_ID));
alter table UserInformation ALTER Authority SET default 'B';
insert into UserInformation values(nextval('UserInformation_user_id_seq'),'管理员','1','A');
insert into UserInformation(User_Name,Password,Authority) values('admin','123456','B');
insert into UserInformation(User_Name,Password,Authority) values('GM','11','B');

CREATE SEQUENCE DepartmentInformation_D_Number_seq;
CREATE TABLE DepartmentInformation(
D_Number int4 NOT NULL DEFAULT nextval('DepartmentInformation_D_Number_seq'),
D_Name varchar(20) NOT NULL,
D_Count int NOT NULL, CONSTRAINT PK_DepartmentInformation PRIMARY KEY(D_Number));
insert into DepartmentInformation(D_Name,D_Count) values('市场部',10);
insert into DepartmentInformation(D_Name,D_Count) values('生产部',200);
insert into DepartmentInformation(D_Name,D_Count) values('财务部',5);
insert into DepartmentInformation(D_Name,D_Count) values('培训部',4);
insert into DepartmentInformation(D_Name,D_Count) values('后勤部',1);
insert into DepartmentInformation(D_Name,D_Count) values('策划部',3);
insert into DepartmentInformation(D_Name,D_Count) values('销售部',15);
insert into DepartmentInformation(D_Name,D_Count) values('食堂',10);
insert into DepartmentInformation(D_Name,D_Count) values('医院',30);

CREATE SEQUENCE EmployeeInformation_E_Number_seq;
CREATE TABLE EmployeeInformation(
E_Number int4 NOT NULL DEFAULT nextval('EmployeeInformation_E_Number_seq'),
E_Name varchar(30) NOT NULL,
E_Sex varchar(2) NOT NULL,
E_BornDate date NOT NULL,
E_Marriage varchar(4) NOT NULL,
E_PoliticsVisage varchar(20) NOT NULL,
E_SchoolAge varchar(20) NULL,
E_EnterDate date NULL,
E_InDueFormDate date NOT NULL,
D_Number int NOT NULL,
E_Headship varchar(20) NOT NULL,
E_Estate varchar(10) NOT NULL,
E_Remark varchar(500) NULL, CONSTRAINT PK_EmployeeInformation PRIMARY KEY(E_Number));
insert into EmployeeInformation values(nextval('EmployeeInformation_E_Number_seq'),'张勇','男',date '1983-5-17','未婚','党员','本科',date '2002-8-17',date '2008-8-17',1,'厂长','在职','');
insert into EmployeeInformation values(nextval('EmployeeInformation_E_Number_seq'),'钱力','男',date '1996-1-2','未婚','党员','本科',date '2004-9-10',date '2006-6-5',2,'厂长','在职','高级工程师');

CREATE SEQUENCE TrainInformation_ID_seq;
CREATE TABLE TrainInformation(
ID int4 NOT NULL DEFAULT nextval('TrainInformation_ID_seq'),
T_Number varchar(20) NOT NULL,
T_Content varchar(100) NOT NULL,
E_Number int NOT NULL,
T_Date int NULL,
T_Money int NULL, CONSTRAINT PK_TrainInformation PRIMARY KEY(ID));
insert into TrainInformation values(nextval('TrainInformation_ID_seq'),'200701','职业素质',2,30,7000);
insert into TrainInformation values(nextval('TrainInformation_ID_seq'),'200702','职业素质',1,31,7000);

CREATE SEQUENCE WageInformation_ID_seq;
CREATE TABLE WageInformation(
ID int4 NOT NULL DEFAULT nextval('WageInformation_ID_seq'),
W_Number int NOT NULL,
E_Number int NOT NULL,
W_BasicWage decimal(18, 2) NOT NULL,
W_Boon decimal(18, 2) NOT NULL,
W_Bonus decimal(18, 2) NOT NULL,
W_FactWage decimal(18, 2) NOT NULL, CONSTRAINT PK_WageInformation PRIMARY KEY(ID));
insert into WageInformation values(nextval('WageInformation_ID_seq'),2,2,2111.00,2300.00,1100.00,5511.00);
insert into WageInformation values(nextval('WageInformation_ID_seq'),3,1,3000.00,2300.00,1200.00,6500.00);

CREATE SEQUENCE RewardspunishmentInformation_ID_seq;
CREATE TABLE RewardspunishmentInformation(
ID int4 NOT NULL DEFAULT nextval('RewardspunishmentInformation_ID_seq'),
R_Number int NOT NULL,
E_Number int NOT NULL,
R_Date date NOT NULL,
R_Address varchar(50) NOT NULL,
R_Causation varchar(200) NOT NULL,
R_Remark varchar(500) NULL, CONSTRAINT PK_EncouragementPunishInf PRIMARY KEY (ID));
insert into RewardspunishmentInformation values(nextval('RewardspunishmentInformation_ID_seq'),1,1,to_date('2005-5-3','YYYY-MM-DD'),'教学馆二楼','演讲比赛一等奖','');	  
insert into RewardspunishmentInformation values(nextval('RewardspunishmentInformation_ID_seq'),2,2,to_date('2004-5-3','YYYY-MM-DD'),'教学馆二楼','演讲比赛一等奖','');	 

ALTER TABLE EmployeeInformation ADD CONSTRAINT FK_EmployeeInf_DepartmentInf FOREIGN KEY(D_Number) 
REFERENCES DepartmentInformation(D_Number);

ALTER TABLE TrainInformation ADD CONSTRAINT FK_TrainInf_EmployeeInf FOREIGN KEY(E_Number) REFERENCES EmployeeInformation(E_Number);

ALTER TABLE WageInformation ADD CONSTRAINT FK_WageInf_EmployeeInf FOREIGN KEY(E_Number) REFERENCES EmployeeInformation(E_Number);

ALTER TABLE RewardspunishmentInformation ADD CONSTRAINT FK_EncouragePInf_EmployeeInf 
FOREIGN KEY(E_Number) REFERENCES EmployeeInformation(E_Number);
