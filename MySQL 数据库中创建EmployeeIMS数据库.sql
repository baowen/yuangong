﻿-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.0.51b-community-nt


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema EmployeeIMS
--

CREATE DATABASE IF NOT EXISTS EmployeeIMS;
USE EmployeeIMS;

--
-- Definition of table `departmentinformation`
--

DROP TABLE IF EXISTS `departmentinformation`;
CREATE TABLE `departmentinformation` (
  `D_Number` int(11) NOT NULL auto_increment,
  `D_Name` varchar(20) NOT NULL,
  `D_Count` int(11) NOT NULL,
  PRIMARY KEY  (`D_Number`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `departmentinformation`
--

/*!40000 ALTER TABLE `departmentinformation` DISABLE KEYS */;
INSERT INTO `departmentinformation` (`D_Number`,`D_Name`,`D_Count`) VALUES 
 (1,'培训部',5),
 (2,'市场部',10),
 (3,'生产部',200),
 (4,'财务部',5),
 (5,'后勤部',1),
 (6,'策划部',3),
 (7,'销售部',15),
 (8,'食堂',10),
 (9,'医院',30);
/*!40000 ALTER TABLE `departmentinformation` ENABLE KEYS */;


--
-- Definition of table `employeeinformation`
--

DROP TABLE IF EXISTS `employeeinformation`;
CREATE TABLE `employeeinformation` (
  `E_Number` int(11) NOT NULL auto_increment,
  `E_Name` varchar(30) NOT NULL,
  `E_Sex` varchar(2) NOT NULL,
  `E_BornDate` date NOT NULL,
  `E_Marriage` varchar(4) NOT NULL,
  `E_PoliticsVisage` varchar(20) NOT NULL,
  `E_SchoolAge` varchar(20) default NULL,
  `E_EnterDate` date default NULL,
  `E_InDueFormDate` date NOT NULL,
  `D_Number` int(11) NOT NULL,
  `E_Headship` varchar(20) NOT NULL,
  `E_Estate` varchar(10) NOT NULL,
  `E_Remark` varchar(500) default NULL,
  PRIMARY KEY  (`E_Number`),
  KEY `FK_EmployeeInf_DepartmentInf` (`D_Number`),
  CONSTRAINT `FK_EmployeeInf_DepartmentInf` FOREIGN KEY (`D_Number`) REFERENCES `departmentinformation` (`D_Number`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `employeeinformation`
--

/*!40000 ALTER TABLE `employeeinformation` DISABLE KEYS */;
INSERT INTO `employeeinformation` (`E_Number`,`E_Name`,`E_Sex`,`E_BornDate`,`E_Marriage`,`E_PoliticsVisage`,`E_SchoolAge`,`E_EnterDate`,`E_InDueFormDate`,`D_Number`,`E_Headship`,`E_Estate`,`E_Remark`) VALUES 
 (1,'张勇','男','1983-05-17','未婚','党员','本科','2002-08-17','2008-08-17',1,'厂长','在职','1'),
 (2,'钱力','男','1996-01-02','未婚','党员','本科','2004-09-10','2006-06-05',2,'厂长','在职','高级工程师'),
 (3,'333','男','2008-09-14','未婚','党员','本科','2008-09-14','2008-09-14',2,'厂长','在职','3');
/*!40000 ALTER TABLE `employeeinformation` ENABLE KEYS */;


--
-- Definition of table `rewardspunishmentinformation`
--

DROP TABLE IF EXISTS `rewardspunishmentinformation`;
CREATE TABLE `rewardspunishmentinformation` (
  `ID` int(11) NOT NULL auto_increment,
  `R_Number` int(11) NOT NULL,
  `E_Number` int(11) NOT NULL,
  `R_Date` date NOT NULL,
  `R_Address` varchar(50) NOT NULL,
  `R_Causation` varchar(200) NOT NULL,
  `R_Remark` varchar(500) default NULL,
  PRIMARY KEY  (`ID`),
  KEY `FK_EncouragePInf_EmployeeInf` (`E_Number`),
  CONSTRAINT `FK_EncouragePInf_EmployeeInf` FOREIGN KEY (`E_Number`) REFERENCES `employeeinformation` (`E_Number`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rewardspunishmentinformation`
--

/*!40000 ALTER TABLE `rewardspunishmentinformation` DISABLE KEYS */;
INSERT INTO `rewardspunishmentinformation` (`ID`,`R_Number`,`E_Number`,`R_Date`,`R_Address`,`R_Causation`,`R_Remark`) VALUES 
 (1,1,1,'2005-05-03','教学馆二楼','演讲比赛一等奖',''),
 (2,2,2,'2004-05-03','教学馆二楼','演讲比赛一等奖','');
/*!40000 ALTER TABLE `rewardspunishmentinformation` ENABLE KEYS */;


--
-- Definition of table `traininformation`
--

DROP TABLE IF EXISTS `traininformation`;
CREATE TABLE `traininformation` (
  `ID` int(11) NOT NULL auto_increment,
  `T_Number` varchar(20) NOT NULL,
  `T_Content` varchar(100) NOT NULL,
  `E_Number` int(11) NOT NULL,
  `T_Date` int(11) default NULL,
  `T_Money` int(11) default NULL,
  PRIMARY KEY  (`ID`),
  KEY `FK_TrainInf_EmployeeInf` (`E_Number`),
  CONSTRAINT `FK_TrainInf_EmployeeInf` FOREIGN KEY (`E_Number`) REFERENCES `employeeinformation` (`E_Number`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `traininformation`
--

/*!40000 ALTER TABLE `traininformation` DISABLE KEYS */;
INSERT INTO `traininformation` (`ID`,`T_Number`,`T_Content`,`E_Number`,`T_Date`,`T_Money`) VALUES 
 (1,'200702','职业素质',1,31,7000),
 (2,'200701','职业素质',2,30,7000);
/*!40000 ALTER TABLE `traininformation` ENABLE KEYS */;


--
-- Definition of table `userinformation`
--

DROP TABLE IF EXISTS `userinformation`;
CREATE TABLE `userinformation` (
  `User_ID` int(11) NOT NULL auto_increment,
  `User_Name` varchar(20) NOT NULL,
  `Password` varchar(20) NOT NULL,
  `Authority` varchar(20) default 'B',
  PRIMARY KEY  (`User_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `userinformation`
--

/*!40000 ALTER TABLE `userinformation` DISABLE KEYS */;
INSERT INTO `userinformation` (`User_ID`,`User_Name`,`Password`,`Authority`) VALUES 
 (1,'管理员','1','A'),
 (2,'admin','123456','B'),
 (3,'GM','11','B'),
 (4,'www','www','B');
/*!40000 ALTER TABLE `userinformation` ENABLE KEYS */;


--
-- Definition of table `wageinformation`
--

DROP TABLE IF EXISTS `wageinformation`;
CREATE TABLE `wageinformation` (
  `ID` int(11) NOT NULL auto_increment,
  `W_Number` int(11) NOT NULL,
  `E_Number` int(11) NOT NULL,
  `W_BasicWage` decimal(18,2) NOT NULL,
  `W_Boon` decimal(18,2) NOT NULL,
  `W_Bonus` decimal(18,2) NOT NULL,
  `W_FactWage` decimal(18,2) NOT NULL,
  PRIMARY KEY  (`ID`),
  KEY `FK_WageInf_EmployeeInf` (`E_Number`),
  CONSTRAINT `FK_WageInf_EmployeeInf` FOREIGN KEY (`E_Number`) REFERENCES `employeeinformation` (`E_Number`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wageinformation`
--

/*!40000 ALTER TABLE `wageinformation` DISABLE KEYS */;
INSERT INTO `wageinformation` (`ID`,`W_Number`,`E_Number`,`W_BasicWage`,`W_Boon`,`W_Bonus`,`W_FactWage`) VALUES 
 (1,2,2,'2111.00','2300.00','1100.00','5511.00'),
 (2,3,1,'3000.00','2300.00','1200.00','6500.00');
/*!40000 ALTER TABLE `wageinformation` ENABLE KEYS */;




/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
