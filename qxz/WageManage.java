//薪筹管理类
package qxz;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.*;
import java.sql.*;

public class WageManage extends JInternalFrame{
	private JLabel lbl7=new JLabel("编　　号：");
   	private JLabel lbl1=new JLabel("员工姓名：");
   	private JLabel lbl2=new JLabel("基本工资：");
   	private JLabel lbl4=new JLabel("福    利：");
   	private JLabel lbl3=new JLabel("奖    金：");
   	//private JLabel lbl5=new JLabel("薪资计算：");
   	private JLabel lbl6=new JLabel("总 薪 资：");

   	private JTextField stid=new JTextField(10);
   	private JTextField stname=new JTextField(10);
   	private JTextField stsalary=new JTextField(10);
   	private JTextField stboon=new JTextField(10);
   	private JTextField stprize=new JTextField(10);
   	//private JTextField stcounter=new JTextField(10);
   	private JTextField stfact=new JTextField(10);
 	ImageIcon icon1=new ImageIcon("image//up.gif");
	ImageIcon icon2=new ImageIcon("image//down.gif");
   	private JButton btnadd=new JButton("添加");
   	private JButton delete=new JButton("删除");
   	private JButton updete=new JButton("修改");
   	private JButton save=new JButton("保存");
   	private JButton up=new JButton("<<");
   	private JButton next=new JButton(">>");
    private JComboBox jComboBox,jComboBoxCode;   	
    private ResultSet rs;

	public WageManage(){
	   	initComponents();
	}
	private void initComponents() {
		setTitle("薪资信息管理");
		Font f=new Font("宋体",Font.PLAIN,12);
    	jComboBox = new JComboBox();
    	//jComboBox.addItem("男");jComboBox.addItem("女");jComboBox.addItem("");
    	jComboBox.setBackground(new Color(204, 204, 204));
    	jComboBox.setPreferredSize(new Dimension(100, 20));

    	jComboBoxCode = new JComboBox();
    	jComboBoxCode.setBackground(new Color(204, 204, 204));
    	jComboBoxCode.setPreferredSize(new Dimension(100, 20));
    	
		getContentPane().setLayout(null);
		lbl7.setBounds(30,30,80,25);
		lbl7.setFont(f);
		getContentPane().add(lbl7);
		stid.setBounds(100,30,100,25);
		stid.setBorder(BorderFactory.createLineBorder(Color.black));
		getContentPane().add(stid);
		stid.setEnabled(false);		
//--------------------------------------------------
		lbl1.setBounds(30,70,80,25);
		lbl1.setFont(f);
		getContentPane().add(lbl1);
		stname.setBounds(100,70,100,25);
		stname.setBorder(BorderFactory.createLineBorder(Color.black));
//		getContentPane().add(stname);		
        getContentPane().add(jComboBox);
        jComboBox.setBounds(100,70,100,25);
        getContentPane().add(jComboBoxCode);
        jComboBoxCode.setBounds(170,70,100,25);
        jComboBoxCode.setVisible(false); 		

        lbl2.setBounds(230,70,80,25);
        lbl2.setFont(f);
        getContentPane().add(lbl2);
        stsalary.setBounds(300,70,100,25);
        stsalary.setBorder(BorderFactory.createLineBorder(Color.black));
        getContentPane().add(stsalary);
        
 //----------------------------------
        lbl3.setBounds(30,110,80,25);
        lbl3.setFont(f);
        getContentPane().add(lbl3);
        stboon.setBounds(100,110,100,25);
        stboon.setBorder(BorderFactory.createLineBorder(Color.black));
        getContentPane().add(stboon);

		lbl4.setBounds(230,110,80,25);
		lbl4.setFont(f);
		getContentPane().add(lbl4);
		stprize.setBounds(300,110,100,25);
		stprize.setBorder(BorderFactory.createLineBorder(Color.black));
		getContentPane().add(stprize);
 //--------------------------------
        //lbl5.setBounds(30,150,80,25);
        //lbl5.setFont(f);
        //getContentPane().add(lbl5);
        //stcounter.setBounds(100,150,100,25);
        //stcounter.setBorder(BorderFactory.createLineBorder(Color.black));
        //getContentPane().add(stcounter);

        lbl6.setBounds(30,150,80,25);
        lbl6.setFont(f);
        getContentPane().add(lbl6);
        stfact.setBounds(100,150,100,25);
        stfact.setBorder(BorderFactory.createLineBorder(Color.black));
        stfact.setToolTipText("基本工资 + 福利 + 奖金");
        getContentPane().add(stfact);
        stfact.setEnabled(false);
 //-------------------------------------------------------
        //按钮
        btnadd.setBounds(30,220,60,25);
        btnadd.setFont(f);
        btnadd.setBorder(BorderFactory.createRaisedBevelBorder());
        getContentPane().add(btnadd);
        delete.setBounds(110,220,60,25);
        delete.setFont(f);
        delete.setBorder(BorderFactory.createRaisedBevelBorder());
        getContentPane().add(delete);
        updete.setBounds(190,220,60,25);
        updete.setFont(f);
        updete.setBorder(BorderFactory.createRaisedBevelBorder());
        getContentPane().add(updete);
        save.setBounds(270,220,60,25);
        save.setFont(f);
        save.setBorder(BorderFactory.createRaisedBevelBorder());
        getContentPane().add(save);
        save.setEnabled(false);

        up.setBounds(350,218,60,15);
        up.setBorder(BorderFactory.createRaisedBevelBorder());
        getContentPane().add(up);
        next.setBounds(350,232,60,15);
        next.setBorder(BorderFactory.createRaisedBevelBorder());
        getContentPane().add(next);
        
        // 更新jComboBox,jComboBoxCode
        //database.joinDB();
		   jComboBox.removeAllItems();
		   try {
			   ResultSet rs2 = Database.executeQuery ("select E_Name,E_Number from EmployeeInformation order by E_Name");
			   int row = Database.recCount (rs2);
			   //从结果集中取出Item加入JComboBox中
			   if(row != 0) rs2.beforeFirst ();
			   for (int i = 0; i < row; i++) {
			  	  rs2.next();
				  jComboBox.addItem (rs2.getString (1));
				  jComboBoxCode.addItem (rs2.getString (2));
		       }
			   jComboBox.addItem("");
			   jComboBoxCode.addItem("");
			   rs2.close();
	       }
	       catch (Exception ex) {
	    	   System.out.println ("initJComboBox (): false");
	       }      
        
        //---连接数据库----------------------------------------------------------------------------
        //database.joinDB();
        //初始化数据--------
        String sqlw="select W_Number,E_Number,W_BasicWage,W_Boon,W_Bonus,W_FactWage from WageInformation";
        try{        	
           rs= Database.executeQuery(sqlw);        	
       	   if(Database.recCount(rs)>0){
           //if(Database.query(sqlw)){
           	  rs.next();
           	  String wNumber=(""+rs.getInt("W_Number"));
           	  String wName=rs.getString("E_Number");
           	  String wBasicWage=rs.getString("W_BasicWage");
           	  String wBoon=rs.getString("W_Boon");
           	  String wBonus=rs.getString("W_Bonus");
           	  //String wCountMethod=rs.getString("W_CountMethod");
           	  String wFactWage=rs.getString("W_FactWage");
           	  stid.setText(wNumber);
           	  stname.setText(wName);
           	  stsalary.setText(wBasicWage);
        	  stboon.setText(wBoon);
        	  stprize.setText(wBonus);
        	  //stcounter.setText(wCountMethod);
        	  stfact.setText(wFactWage);
           }
//           // 更新jComboBox,jComboBoxCode
//   		   jComboBox.removeAllItems();
//   		   try {
//   			   ResultSet rs2 = Database.executeQuery ("select E_Name,E_Number from EmployeeInformation order by E_Name");
//   			   int row = Database.recCount (rs2);
//   			   //从结果集中取出Item加入JComboBox中
//   			   if(row != 0) rs2.beforeFirst ();
//   			   for (int i = 0; i < row; i++) {
//   			  	  rs2.next();
//   				  jComboBox.addItem (rs2.getString (1));
//   				  jComboBoxCode.addItem (rs2.getString (2));
//   		       }
//   			   jComboBox.addItem("");
//   			   jComboBoxCode.addItem("");
//   			   rs2.close();
//   	       }
//   	       catch (Exception ex) {
//   	    	   System.out.println ("initJComboBox (): false");
//   	       }      
   	       jComboBoxCode.setSelectedItem(stname.getText());
   	       jComboBox.setSelectedIndex(jComboBoxCode.getSelectedIndex());           
        }
        catch(Exception esql){
        	System.out.println("薪资信息管理-初始化数据失败!");
        }
        
       stsalary.addActionListener(new ActionListener(){
    	   public void actionPerformed(ActionEvent e){
      		   stfact.setText(Float.toString(Float.parseFloat(stsalary.getText())+Float.parseFloat(stboon.getText())+Float.parseFloat(stprize.getText())));
    	   }
       });
       stsalary.addMouseListener(new MouseListener(){
	     public void mouseEntered(MouseEvent e){
   		     stfact.setText(Float.toString(Float.parseFloat(stsalary.getText())+Float.parseFloat(stboon.getText())+Float.parseFloat(stprize.getText())));
	     }
	     public void mouseExited(MouseEvent e){
    		 stfact.setText(Float.toString(Float.parseFloat(stsalary.getText())+Float.parseFloat(stboon.getText())+Float.parseFloat(stprize.getText())));
	     }
	     public void mousePressed(MouseEvent e){} 
	     public void mouseReleased(MouseEvent e){} 
	     public void mouseClicked(MouseEvent e){}
       });

       stboon.addActionListener(new ActionListener(){
    	   public void actionPerformed(ActionEvent e){
      		   stfact.setText(Float.toString(Float.parseFloat(stsalary.getText())+Float.parseFloat(stboon.getText())+Float.parseFloat(stprize.getText())));
    	   }
       });
       stboon.addMouseListener(new MouseListener(){
	     public void mouseEntered(MouseEvent e){
   		     stfact.setText(Float.toString(Float.parseFloat(stsalary.getText())+Float.parseFloat(stboon.getText())+Float.parseFloat(stprize.getText())));
	     }
	     public void mouseExited(MouseEvent e){
    		 stfact.setText(Float.toString(Float.parseFloat(stsalary.getText())+Float.parseFloat(stboon.getText())+Float.parseFloat(stprize.getText())));
	     }
	     public void mousePressed(MouseEvent e){} public void mouseReleased(MouseEvent e){} public void mouseClicked(MouseEvent e){}
       });
       stprize.addActionListener(new ActionListener(){
    	   public void actionPerformed(ActionEvent e){
      		   stfact.setText(Float.toString(Float.parseFloat(stsalary.getText())+Float.parseFloat(stboon.getText())+Float.parseFloat(stprize.getText())));
    	   }
       });
       stprize.addMouseListener(new MouseListener(){
	     public void mouseEntered(MouseEvent e){
   		     stfact.setText(Float.toString(Float.parseFloat(stsalary.getText())+Float.parseFloat(stboon.getText())+Float.parseFloat(stprize.getText())));
	     }
	     public void mouseExited(MouseEvent e){
    		 stfact.setText(Float.toString(Float.parseFloat(stsalary.getText())+Float.parseFloat(stboon.getText())+Float.parseFloat(stprize.getText())));
	     }
	     public void mousePressed(MouseEvent e){} public void mouseReleased(MouseEvent e){} public void mouseClicked(MouseEvent e){}
       });
       
 //------按钮事件--------------------------------------------------------------------------
       up.addActionListener(new ActionListener(){
          public void actionPerformed(ActionEvent e){
          	try{
	          	if(rs.previous()){
		           String wNumber=(""+rs.getString("W_Number"));
		           String wName=rs.getString("E_Number");
	        	   Database.setJComboBox(jComboBoxCode,rs.getString("E_Number"));
	        	   //jComboBox.setSelectedItem(jComboBoxCode.getSelectedIndex());
	        	   jComboBox.setSelectedIndex(jComboBoxCode.getSelectedIndex());
		           
		           String wBasicWage=rs.getString("W_BasicWage");
		           String wBoon=rs.getString("W_Boon");
		           String wBonus=rs.getString("W_Bonus");
		           //String wCountMethod=rs.getString("W_CountMethod");
		           String wFactWage=rs.getString("W_FactWage");
		           stid.setText(wNumber);
		           stname.setText(wName);
		           stsalary.setText(wBasicWage);
		           stboon.setText(wBoon);
		           stprize.setText(wBonus);
		           //stcounter.setText(wCountMethod);
		           stfact.setText(wFactWage);
	          	}
    			else{
			        new JOptionPane().showMessageDialog(null,"已到头了！");
    			}
          	}
          	catch(Exception eup){
          	    //System.out.println("以到最前一条!");
          	}
          }
       });
       next.addActionListener(new ActionListener(){
           public void actionPerformed(ActionEvent e){
           	 try{
	           	 if(rs.next()){
	           	    String wNumber=(""+rs.getString("W_Number"));
	           	    String wName=rs.getString("E_Number");
	        		jComboBoxCode.setSelectedItem(rs.getString("E_Number"));
	        	    jComboBox.setSelectedIndex(jComboBoxCode.getSelectedIndex());
	           	    
	           	    String wBasicWage=rs.getString("W_BasicWage");
	           	    String wBoon=rs.getString("W_Boon");
	           	    String wBonus=rs.getString("W_Bonus");
	           	    //String wCountMethod=rs.getString("W_CountMethod");
	           	    String wFactWage=rs.getString("W_FactWage");
	           	    stid.setText(wNumber);
	           	    stname.setText(wName);
	           	    stsalary.setText(wBasicWage);
	        	    stboon.setText(wBoon);
	        	    stprize.setText(wBonus);
	        	    //stcounter.setText(wCountMethod);
	        	    stfact.setText(wFactWage);
	           	 }
     			 else{
				    new JOptionPane().showMessageDialog(null,"已到尾了！");
				    rs.last();
    			 }
          	 }
           	 catch(Exception enext){
           	 	System.out.println("以到最后一条");
           	 }
           }
       });
     //为添加删除保存修改按钮加事件----------------------------------------
     //添加
     btnadd.addActionListener(new ActionListener(){
        public void actionPerformed(ActionEvent e){
           	save.setEnabled(true);
           	stid.setText("自动生成");
           	stid.setEditable(false);
           	//stname.setText("");
           	stsalary.setText("0");
        	stboon.setText("0");
        	stprize.setText("0");
        	//stcounter.setText("");
        	stfact.setText("0");
        }
     });
     // 组合框，选项事件
     jComboBox.addItemListener(new ItemListener() { 
     	public void itemStateChanged(ItemEvent e) { 
    	      jComboBoxCode.setSelectedIndex(jComboBox.getSelectedIndex());
     	} 
     });      
     //保存
     save.addActionListener(new ActionListener(){
        public void actionPerformed(ActionEvent e){
        	if(stsalary.getText().equals("")||stboon.getText().equals("")||
        	stprize.getText().equals("")||stfact.getText().equals("")){
        		System.out.println("以上信息必须填写!");
        	}
        	else{        		
			  String sql="select max(W_Number)+1 from WageInformation";
			  int maxnum=1;
        	  try {
        		 ResultSet rs2 = Database.executeQuery(sql);
        		 if(rs2.next()) maxnum=rs2.getInt(1);
        		 rs2.close();
        	  }
        	  catch (Exception ex) {
        	  	 System.out.println (ex);
        	  }
      	      jComboBoxCode.setSelectedIndex(jComboBox.getSelectedIndex());        	  
        		
           	 // String wNumber=stid.getText();
           	  String wName=stname.getText();
           	  String wBasicWage=stsalary.getText();
           	  String wBoon=stboon.getText();
           	  String wBonus=stprize.getText();
           	  //String wCountMethod=stcounter.getText();
           	  String wFactWage=stfact.getText();
              ////////////////////////////////////////////////// 下命令需要完善的        	
     	  	  String Insert = "";
    		  if(Database.dbms.equals("Sql Server")){
    			  Insert="insert WageInformation(W_Number,E_Number,W_BasicWage,W_Boon,W_Bonus,W_FactWage) values("+Integer.toString(maxnum).trim()+","+jComboBoxCode.getSelectedItem()+",'"+wBasicWage+"','"+wBoon+"','"+wBonus+"','"+wFactWage+"')";
    		  }
    		  if(Database.dbms.equals("Oracle")){
    			  Insert="insert into WageInformation(ID,W_Number,E_Number,W_BasicWage,W_Boon,W_Bonus,W_FactWage) values(SWI.nextval,"+Integer.toString(maxnum).trim()+","+jComboBoxCode.getSelectedItem()+",'"+wBasicWage+"','"+wBoon+"','"+wBonus+"','"+wFactWage+"')";
    		  }
    		  if(Database.dbms.equals("MySQL")){
    			  Insert="insert into WageInformation(ID,W_Number,E_Number,W_BasicWage,W_Boon,W_Bonus,W_FactWage) values(null,"+Integer.toString(maxnum).trim()+","+jComboBoxCode.getSelectedItem()+",'"+wBasicWage+"','"+wBoon+"','"+wBonus+"','"+wFactWage+"')";
    		  }
    		  if(Database.dbms.equals("PostgreSQL")){
    			  Insert="insert into WageInformation(ID,W_Number,E_Number,W_BasicWage,W_Boon,W_Bonus,W_FactWage) values(nextval('WageInformation_ID_seq'),"+Integer.toString(maxnum).trim()+","+jComboBoxCode.getSelectedItem()+",'"+wBasicWage+"','"+wBoon+"','"+wBonus+"','"+wFactWage+"')";
    		  }
           	  try{
              	if(Database.executeUpdate(Insert)!=0){
              		stid.setEditable(true);
              		save.setEnabled(false);
              		new JOptionPane().showMessageDialog(null,"添加数据成功！");
              		//Database.joinDB();
              		sql="select W_Number,E_Number,W_BasicWage,W_Boon,W_Bonus,W_FactWage from WageInformation";
                	rs= Database.executeQuery(sql);
              		//Database.query(sql);
              		rs.last();

		            String wNumber1=(""+rs.getString("W_Number"));
		            String wName1=rs.getString("E_Number");
	        		jComboBoxCode.setSelectedItem(rs.getString("E_Number"));
	        	    jComboBox.setSelectedIndex(jComboBoxCode.getSelectedIndex());
		            
		            String wBasicWage1=rs.getString("W_BasicWage");
		            String wBoon1=rs.getString("W_Boon");
		            String wBonus1=rs.getString("W_Bonus");
		            //String wCountMethod1=rs.getString("W_CountMethod");
		            String wFactWage1=rs.getString("W_FactWage");
		            stid.setText(wNumber1);
		            stname.setText(wName1);
		            stsalary.setText(wBasicWage1);
		        	stboon.setText(wBoon1);
		        	stprize.setText(wBonus1);
		        	//stcounter.setText(wCountMethod1);
		        	stfact.setText(wFactWage1);
                }
                else{
   	  				new JOptionPane().showMessageDialog(null,"添加数据不成功！");
                }
              }
              catch(Exception esave){
            	    System.out.println(esave);
   	  				new JOptionPane().showMessageDialog(null,"添加数据不成功222！");
              }
          	  save.setEnabled(false);
         	}
        }
     });
   //删除
   delete.addActionListener(new ActionListener(){
   	  public void actionPerformed(ActionEvent edel){
   	  	try{
   	  	   String sqle="delete from WageInformation where W_Number ='"+stid.getText()+"'";
   	  	   //System.out.println(sqle);   	  	
	       if(Database.executeUpdate(sqle)!=0){
   	       //if(Database.executeSQL(sqle)){
   			  new JOptionPane().showMessageDialog(null,"数据删除成功！");
   			  //Database.joinDB();
   	  		  String sqll="select W_Number,E_Number,W_BasicWage,W_Boon,W_Bonus,W_FactWage from WageInformation";
         	  rs= Database.executeQuery(sqll);
   	  		  //Database.query(sqll);
   	  		  rs.last();
           	  String wNumber1=(""+rs.getString("W_Number"));
           	  String wName1=rs.getString("E_Number");
      		  jComboBoxCode.setSelectedItem(rs.getString("E_Number"));
    	      jComboBox.setSelectedIndex(jComboBoxCode.getSelectedIndex());
           	  
           	  String wBasicWage1=rs.getString("W_BasicWage");
           	  String wBoon1=rs.getString("W_Boon");
           	  String wBonus1=rs.getString("W_Bonus");
           	  //String wCountMethod1=rs.getString("W_CountMethod");
           	  String wFactWage1=rs.getString("W_FactWage");
	          stid.setText(wNumber1);
           	  stname.setText(wName1);
           	  stsalary.setText(wBasicWage1);
        	  stboon.setText(wBoon1);
        	  stprize.setText(wBonus1);
        	  //stcounter.setText(wCountMethod1);
        	  stfact.setText(wFactWage1);
   	       }
   	   }
   	   catch(Exception edelete){
   	   	  System.out.println("数据删除失败!");
   	   }
   	  }
   });
   //修改
   updete.addActionListener(new ActionListener(){
      public void actionPerformed(ActionEvent eupd){
      	 try{
   			String supdate="update WageInformation set E_Number="+ jComboBoxCode.getSelectedItem()+",W_BasicWage='"+stsalary.getText() +"',W_Boon='"+stboon.getText()+"',W_Bonus='"+stprize.getText()+"',W_FactWage='"+stfact.getText()+"' where W_Number='"+ stid.getText()+"'";
 			if(Database.executeUpdate(supdate)!=0){
      	 	//if(Database.executeSQL(supdate)){
   			   new JOptionPane().showMessageDialog(null,"数据修改成功！");
   			   //Database.joinDB();
   	  		   String sqll="select W_Number,E_Number,W_BasicWage,W_Boon,W_Bonus,W_FactWage from WageInformation";
	           rs= Database.executeQuery(sqll);
   	  		   //Database.query(sqll);
   	  		   rs.last();
           	   String wNumber1=(""+rs.getString("W_Number"));
           	   String wName1=rs.getString("E_Number");
          	   jComboBoxCode.setSelectedItem(rs.getString("E_Number"));
        	   jComboBox.setSelectedIndex(jComboBoxCode.getSelectedIndex());
           	   
           	   String wBasicWage1=rs.getString("W_BasicWage");
           	   String wBoon1=rs.getString("W_Boon");
           	   String wBonus1=rs.getString("W_Bonus");
           	   //String wCountMethod1=rs.getString("W_CountMethod");
           	   String wFactWage1=rs.getString("W_FactWage");
	           stid.setText(wNumber1);
           	   stname.setText(wName1);
           	   stsalary.setText(wBasicWage1);
        	   stboon.setText(wBoon1);
        	   stprize.setText(wBonus1);
        	   //stcounter.setText(wCountMethod1);
        	   stfact.setText(wFactWage1);
        	}
      	 }
      	 catch(Exception eupdete){
      	 	System.out.println("修改数据失败!");
      	 }
      }
   });
//---------------------------------------------------------------------------------
	setSize(460,320);
	this.setClosable(true);
	setVisible(true);
	}
}
