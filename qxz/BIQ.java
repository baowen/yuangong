//员工信息查询类
package qxz;

import java.awt.*;
import javax.swing.*;
import javax.swing.table.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.JScrollPane.*;
import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class BIQ extends JInternalFrame{
	JLabel lbl1=new JLabel("基 本 信 息 查 询");
   	JLabel lbl2=new JLabel("员工编号：");
   	JLabel lbl3=new JLabel("员工姓名：");
   	JTextField btxtid=new JTextField(10);
   	JTextField btxtname=new JTextField(10);
   	JButton btn1=new JButton("查询");
    private ResultSet rs;
    SimpleDateFormat sdf=new SimpleDateFormat( "yyyy-MM-dd"); 

   	JTable table;
	DefaultTableModel dtm;

    String columns[] = {"员工编号","员工姓名"," 性别 ","出生日期","婚姻状况","政治面貌"," 学历 ","进入公司时间","转正时间"," 部门 "," 职务 ","员工状态"," 备注 "};
	public BIQ(){
		setTitle("基 本 信 息 查 询");

		dtm = new DefaultTableModel();
		table = new JTable(dtm);

		JScrollPane sl = new JScrollPane();
		sl.getViewport().add(table);
		dtm.setColumnCount(5);
		dtm.setColumnIdentifiers(columns);
	    
		getContentPane().setLayout(null);
		lbl1.setBounds(300,10,300,30);
		lbl1.setFont(new Font("宋体",Font.BOLD,24));
		getContentPane().add(lbl1);

		Font f=new Font("宋体",Font.PLAIN,12);
		lbl2.setBounds(10,60,80,25);
		lbl2.setFont(f);
		getContentPane().add(lbl2);
		btxtid.setBounds(80,60,80,23);
		btxtid.setFont(f);
		getContentPane().add(btxtid);
		lbl3.setBounds(10,90,80,25);
		lbl3.setFont(f);
   	    getContentPane().add(lbl3);
   	    btxtname.setBounds(80,90,80,23);
   	    btxtname.setFont(f);
   	    getContentPane().add(btxtname);
   	    btn1.setBounds(90,130,60,25);
   	    btn1.setFont(f);
   	    getContentPane().add(btn1);

		sl.setBounds(180,60,620,370);
		getContentPane().add(sl);

		//设置边框
        btxtid.setBorder(BorderFactory.createLineBorder(Color.black));
   	    btxtname.setBorder(BorderFactory.createLineBorder(Color.black));
   	    btn1.setBorder(BorderFactory.createRaisedBevelBorder());
        sl.setBorder(BorderFactory.createLineBorder(Color.black));
        
        //----连接数据库--------------------------------------------------------------------------
		//database.joinDB();
		String sql="select E_Number,E_Name,E_Sex,E_BornDate,E_Marriage,E_PoliticsVisage,E_SchoolAge,E_EnterDate,E_InDueFormDate,d.D_Name as E_Department,E_Headship,E_Estate,E_Remark from EmployeeInformation e,DepartmentInformation d where e.D_Number=d.D_Number";

        rs=Database.executeQuery(sql);
        //if(Database.query(sql)){
        if(Database.recCount(rs)>0){
		   //System.out.println(sql);
		   try{
		   	  while(rs.next()){
		   	  	 String eNumber=(""+rs.getInt("E_Number"));
//		   	  	 System.out.println(eNumber);
		   	  	 String eName=rs.getString("E_Name");
//		   	  	 System.out.println(eName);
		   	  	 String eSex=rs.getString("E_Sex");
//		   	  	 System.out.println(eSex);
		   	  	 String eBornDate=sdf.format(rs.getDate("E_BornDate"));
//		   	  	 System.out.println(eBornDate);
		   	  	 String eMarriage=rs.getString("E_Marriage");
//		   	  	 System.out.println(eMarriage);
		   	  	 String ePoliticsVisage=rs.getString("E_PoliticsVisage");
//		   	  	 System.out.println(ePoliticsVisage);
		   	  	 String eSchoolAge=rs.getString("E_SchoolAge");
//		   	  	 System.out.println(eSchoolAge);
		   	  	 String eEnterDate=sdf.format(rs.getDate("E_EnterDate"));
//		   	  	 System.out.println(eEnterDate);
		   	  	 String eInDueFormDate=sdf.format(rs.getDate("E_InDueFormDate"));
//		   	  	 System.out.println(eInDueFormDate);
		   	  	 String eDepartment=rs.getString("E_Department");
//		   	  	 System.out.println(eDepartment);
		   	  	 String eHeadship=rs.getString("E_Headship");
//		   	  	 System.out.println(eHeadship);
		   	  	 String eEstate=rs.getString("E_Estate");
//		   	  	 System.out.println(eEstate);
		   	  	 String eRemark=rs.getString("E_Remark");
//		   	  	 System.out.println(eRemark);

		   	     Vector v=new Vector();
		   	     v.add(eNumber);
				 v.add(eName);
				 v.add(eSex);
				 v.add(eBornDate);
				 v.add(eMarriage);
				 v.add(ePoliticsVisage);
				 v.add(eSchoolAge);
		   	     v.add(eEnterDate);
				 v.add(eInDueFormDate);
				 v.add(eDepartment);
				 v.add(eHeadship);
				 v.add(eEstate);
				 v.add(eRemark);
				 dtm.addRow(v);
		   	  }
		   }
		   catch(Exception eBIQ){
		      System.out.println("初始化数据失败！");
		   }
		}
        //为查询按钮加事件--------------------------------------------------------
		btn1.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent eBIQ){
				System.out.println("按钮事件");
				String esql;

        		int rc=dtm.getRowCount();
        		for(int i=0;i<rc;i++){
        			dtm.removeRow(0);
        		}
				if(btxtid.getText().equals("")&&btxtname.getText().equals("")){
				   esql="select E_Number,E_Name,E_Sex,E_BornDate,E_Marriage,E_PoliticsVisage,E_SchoolAge,E_EnterDate,E_InDueFormDate,d.D_Name as E_Department,E_Headship,E_Estate,E_Remark from EmployeeInformation e,DepartmentInformation d where e.D_Number=d.D_Number";
				}
				else if(btxtname.getText().equals("")){
					esql="select E_Number,E_Name,E_Sex,E_BornDate,E_Marriage,E_PoliticsVisage,E_SchoolAge,E_EnterDate,E_InDueFormDate,d.D_Name as E_Department,E_Headship,E_Estate,E_Remark from EmployeeInformation e,DepartmentInformation d where e.D_Number=d.D_Number and E_Number = '" + btxtid.getText() +"'";
					}
				else{
        		   esql="select E_Number,E_Name,E_Sex,E_BornDate,E_Marriage,E_PoliticsVisage,E_SchoolAge,E_EnterDate,E_InDueFormDate,d.D_Name as E_Department,E_Headship,E_Estate,E_Remark from EmployeeInformation e,DepartmentInformation d where e.D_Number=d.D_Number and ( E_Number = '" + btxtid.getText() +"' or E_Name like '%"+ btxtname.getText().trim()+"%')";				}

    		    rs=Database.executeQuery(esql);
    			if(Database.recCount(rs)>0){
				//if(Database.query(esql)){
				   try{
				   	  while(rs.next()){
				   	  	 String eNumber=(""+rs.getInt("E_Number"));
				   	  	 //System.out.println(eNumber);
				   	  	 String eName=rs.getString("E_Name");
				   	  	 //System.out.println(eName);
				   	  	 String eSex=rs.getString("E_Sex");
				   	  	 //System.out.println(eSex);
				   	  	 String eBornDate=sdf.format(rs.getDate("E_BornDate"));
				   	  	 //System.out.println(eBornDate);
				   	  	 String eMarriage=rs.getString("E_Marriage");
				   	  	 //System.out.println(eMarriage);
				   	  	 String ePoliticsVisage=rs.getString("E_PoliticsVisage");
				   	  	 //System.out.println(ePoliticsVisage);
				   	  	 String eSchoolAge=rs.getString("E_SchoolAge");
				   	  	 //System.out.println(eSchoolAge);
				   	  	 String eEnterDate=sdf.format(rs.getDate("E_EnterDate"));
				   	  	 //System.out.println(eEnterDate);
				   	  	 String eInDueFormDate=sdf.format(rs.getDate("E_InDueFormDate"));
				   	  	 //System.out.println(eInDueFormDate);
				   	  	 String eDepartment=rs.getString("E_Department");
				   	  	 //System.out.println(eDepartment);
				   	  	 String eHeadship=rs.getString("E_Headship");
				   	  	 //System.out.println(eHeadship);
				   	  	 String eEstate=rs.getString("E_Estate");
				   	  	 //System.out.println(eEstate);
				   	  	 String eRemark=rs.getString("E_Remark");
				   	  	 //System.out.println(eRemark);

				   	     Vector v=new Vector();
				   	     v.add(eNumber);
						 v.add(eName);
						 v.add(eSex);
						 v.add(eBornDate);
						 v.add(eMarriage);
						 v.add(ePoliticsVisage);
						 v.add(eSchoolAge);
				   	     v.add(eEnterDate);
						 v.add(eInDueFormDate);
						 v.add(eDepartment);
						 v.add(eHeadship);
						 v.add(eEstate);
						 v.add(eRemark);
						 dtm.addRow(v);
				   	  }
				   }
				   catch(Exception eB){
				   }
				}
			}
		});
        //------------------------------------------------------------------
    	setSize(820,480);
		this.setClosable(true);
		setVisible(true);
	}
}