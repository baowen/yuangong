//用户登陆类
package qxz;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

//import java.sql.*;

public class Login extends JFrame{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	JFrame jf ;

	JTextField textName=new JTextField("admin");
	JPasswordField textPassword=new JPasswordField("123456");

	JLabel label = new JLabel("企业员工管理系统");
	JLabel labelName=new JLabel("用户名：");
	JLabel labelPassword=new JLabel("密　码：");

	JButton buttonEnter=new JButton("登录");
	JButton buttoncancel=new JButton("清空");

	public Login(){
		jf=this;
		setTitle("登录");
		Font f = new Font("新宋体",Font.PLAIN,12);
		Container con = getContentPane();
		con.setLayout(null);
		label.setBounds(80,10,140,20);
		label.setFont(new Font("新宋体",Font.BOLD,16));
		con.add(label);
		labelName.setBounds(55,45,55,20);
		labelName.setFont(f);
		con.add(labelName);
		textName.setBounds(105,45,120,20);
		con.add(textName);

        labelPassword.setBounds(55,75,55,20);
		con.add(labelPassword);
		labelPassword.setFont(f);
     	textPassword.setBounds(105,75,120,20);
		con.add(textPassword);

		buttonEnter.setBounds(90,115,60,20);
		buttonEnter.setFont(f);
		con.add(buttonEnter);
		buttonEnter.setFocusable(true);
		
		//登陆的鼠标监听
		buttonEnter.addMouseListener(new MouseAdapter(){
			public void mouseClicked(MouseEvent me){
				if(textName.getText().equals("")){
					new JOptionPane().showMessageDialog(null,"用户名不能为空!");
				}
				else if(textPassword.getText().equals("")){
					new JOptionPane().showMessageDialog(null,"密码不能为空!");
				}
				else{
					String sql="select * from UserInformation where User_Name = '" + textName.getText() + "' and Password = '" + textPassword.getText()+ "'";
					//sql="select * from UserInformation ";
					System.out.println(sql);
					Judge(sql);
				}
			}
		});
		buttonEnter.addKeyListener(new KeyAdapter(){
	       public void keyPressed(KeyEvent e){
				if(textName.getText().equals("")){
					new JOptionPane().showMessageDialog(null,"用户名不能为空!");
				}
				else if(textPassword.getText().equals("")){
					new JOptionPane().showMessageDialog(null,"密码不能为空!");
				}
				else{
					String sql="select * from UserInformation where User_Name = '" + textName.getText() + "' and Password = '" + textPassword.getText()+ "'";
					System.out.println(sql);
					Judge(sql);
				}
	       }
	    });		
			
		buttoncancel.setBounds(155,115,60,20);
		buttoncancel.setFont(f);
		con.add(buttoncancel);
		//清空按钮的鼠标监听方法
		buttoncancel.addMouseListener(new MouseAdapter(){
			public void mouseClicked(MouseEvent me){
				textName.setText("");
				textPassword.setText("");
			}
		});
		
        //窗口大小不可调
		setResizable(false);
        //窗口图标
        Image img=Toolkit.getDefaultToolkit().getImage("image\\main.gif");
        setIconImage(img);
        Toolkit t = Toolkit.getDefaultToolkit();
        int w = t.getScreenSize().width;
        int h = t.getScreenSize().height;
        setBounds(w/2-150,h/2-90,300,180);
        setVisible(true);
        
        // 获取焦点
        buttonEnter.grabFocus();
		buttonEnter.requestFocusInWindow();		
		//labelPassword.grabFocus();
		//labelPassword.requestFocusInWindow();
		//jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	private void Judge(String sqlString) {
	    if (Database.joinDB()) {
	      if (Database.query(sqlString))
	         try{
	        	//Database.rs.first();
                //System.out.println(sqlString);                
                //System.out.println(Database.rs.getString(1).toString());                
	        	//Database.rs.beforeFirst();
	            if(Database.rs.isBeforeFirst()) {
	               System.out.println("密码正确");
	               jf.setVisible(false);
	               //关闭数据库连接
	               //Database.cn.close();
	               new Main();
	            }
	            else {
	               System.out.println("错误");
	               new JOptionPane().showMessageDialog(null,"用户名或密码错误!","",JOptionPane.ERROR_MESSAGE);//!!!!!!!!!!!!!!
	            }
	          }catch(Exception ex) {
	             System.out.println(ex.getMessage());
	          }
	    }
	    else{
	   	   System.out.println("连接数据库不成功!!!");
	   	}
    }

	public static void main(String args[]){
		new Login();
	}
}