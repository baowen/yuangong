//奖惩管理类
package qxz;

import java.awt.*;

import javax.swing.*;
import javax.swing.text.DateFormatter;

import java.awt.event.*;
//import java.sql.*;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Rewardspunishment extends JInternalFrame {
    private JComboBox jComboBox,jComboBoxCode;
    private String sql;
    private ResultSet rs;
    public Rewardspunishment() {
        //连接数据库--------
        //Database.joinDB();
    	setTitle("员工奖惩管理!");
    	Font f = new Font("新宋体", 0, 14);
        lb1 = new JLabel("员 工 奖 罚 信 息");
        lb2 = new JLabel("奖惩编号：");
        lb3 = new JLabel("地    点:");
        lb4 = new JLabel("奖惩时间:");
        lb5 = new JLabel("奖惩原因:");
        lb6 = new JLabel("备注:");
        lb7 = new JLabel("员工姓名:");
        tnumber = new JTextField();
        tname = new JTextField();
        tremarks = new JTextArea();
        tadress = new JTextField();
        treason = new JTextField();
        //ttime = new JTextField();
        btright = new JButton(">>");
        btleft = new JButton("<<");
        btdelet = new JButton("删除");
        btamend = new JButton("修改");
        btsave = new JButton("保存");
        btsave.setEnabled(false);
        btadd = new JButton("添加");
        
       	//定义与初始化组合框
    	jComboBox = new JComboBox();
    	//jComboBox.addItem("男");jComboBox.addItem("女");jComboBox.addItem("");
    	jComboBox.setBackground(new Color(204, 204, 204));
    	jComboBox.setPreferredSize(new Dimension(100, 20));

    	jComboBoxCode = new JComboBox();
    	jComboBoxCode.setBackground(new Color(204, 204, 204));
    	jComboBoxCode.setPreferredSize(new Dimension(100, 20));        

        getContentPane().setLayout(null);

        lb1.setBackground(new java.awt.Color(204, 204, 204));
        lb1.setFont(new java.awt.Font("新宋体", 1, 18));
        lb1.setForeground(new java.awt.Color(0, 0, 255));
        getContentPane().add(lb1);
        lb1.setBounds(120, 10, 190, 30);

        lb2.setFont(f);
        getContentPane().add(lb2);
        lb2.setBounds(20, 60, 70, 20);

        lb7.setFont(f);
        getContentPane().add(lb7);
        lb7.setBounds(215, 60, 70, 20);

        //getContentPane().add(tname);
        //tname.setBounds(280,60,90,20);
        getContentPane().add(jComboBox);
        jComboBox.setBounds(280,60,90,20);
        getContentPane().add(jComboBoxCode);
        jComboBoxCode.setBounds(340,60,90,20);
        jComboBoxCode.setVisible(false);        
        
        lb3.setFont(f);
        getContentPane().add(lb3);
        lb3.setBounds(20, 100, 70, 20);

        lb4.setFont(f);
        getContentPane().add(lb4);
        lb4.setBounds(20, 140, 70, 20);

        lb5.setFont(f);

        getContentPane().add(lb5);
        lb5.setBounds(20, 180, 70, 20);

        lb6.setFont(f);

        getContentPane().add(lb6);
        lb6.setBounds(260, 100, 70, 20);

        getContentPane().add(tnumber);
        tnumber.setBounds(100, 60, 90, 20);
        tnumber.setEnabled(false);		

        getContentPane().add(tremarks);
        tremarks.setBounds(260, 130, 130, 70);

        getContentPane().add(tadress);
        tadress.setBounds(100, 100, 150, 20);

        getContentPane().add(treason);
        treason.setBounds(100, 180, 150, 21);

        //ttime.setBounds(100, 140, 150, 20);
        //getContentPane().add(ttime);
        ttime.setValue(new Date());
        getContentPane().add(ttime);
        ttime.setBounds(100, 140, 150, 20);

        btright.setFont(f);
        getContentPane().add(btright);
        btright.setBounds(330, 250, 50, 20);

        btleft.setFont(f);
        getContentPane().add(btleft);
        btleft.setBounds(330, 230, 50, 20);

        btdelet.setFont(f);
        getContentPane().add(btdelet);
        btdelet.setBounds(250, 240, 70, 25);

        btamend.setFont(f);
        getContentPane().add(btamend);
        btamend.setBounds(170, 240, 70, 25);

        btsave.setFont(f);
        getContentPane().add(btsave);
        btsave.setBounds(10, 240, 70, 25);

        btadd.setFont(f);
        getContentPane().add(btadd);
        btadd.setBounds(90, 240, 70, 25);
        
        // 更新jComboBox,jComboBoxCode
		   jComboBox.removeAllItems();
		   try {
			  ResultSet rs2 = Database.executeQuery ("select E_Name,E_Number from EmployeeInformation order by E_Name");
			  int row = Database.recCount (rs2);
			   //从结果集中取出Item加入JComboBox中
			   if(row != 0) rs2.beforeFirst ();
			   for (int i = 0; i < row; i++) {
				   rs2.next();
				   jComboBox.addItem (rs2.getString (1));
				   jComboBoxCode.addItem (rs2.getString (2));
		       }
			   jComboBox.addItem("");
			   jComboBoxCode.addItem("");
			   rs2.close();
	       }
	       catch (Exception ex) {
	    	  System.out.println ("initJComboBox (): false");
	       }      
        
//初始化窗体数据-------------------------------------------------------
        String sqlc="select R_Number,E_Number,R_Date,R_Address,R_Causation,R_Remark from RewardspunishmentInformation";
        try{
            rs= Database.executeQuery(sqlc);        	
        	if(Database.recCount(rs)>0){
            //if(Database.query(sqlc)){
        	   rs.next();
               tnumber.setText(""+rs.getInt("R_Number"));
               tname.setText(rs.getString("E_Number"));
               //ttime.setText(rs.getString("R_Date"));
               ttime.setValue(rs.getDate("R_Date"));
               tadress.setText(rs.getString("R_Address"));
               treason.setText(rs.getString("R_Causation"));
               tremarks.setText(rs.getString("R_Remark"));
            
//               // 更新jComboBox,jComboBoxCode
//    		   jComboBox.removeAllItems();
//    		   try {
//    			  ResultSet rs2 = Database.executeQuery ("select E_Name,E_Number from EmployeeInformation order by E_Name");
//    			  int row = Database.recCount (rs2);
//    			   //从结果集中取出Item加入JComboBox中
//    			   if(row != 0) rs2.beforeFirst ();
//    			   for (int i = 0; i < row; i++) {
//    				   rs2.next();
//    				   jComboBox.addItem (rs2.getString (1));
//    				   jComboBoxCode.addItem (rs2.getString (2));
//    		       }
//    			   jComboBox.addItem("");
//    			   jComboBoxCode.addItem("");
//    			   rs2.close();
//    	       }
//    	       catch (Exception ex) {
//    	    	  System.out.println ("initJComboBox (): false");
//    	       }      
    	       jComboBoxCode.setSelectedItem(tname.getText());
    	       jComboBox.setSelectedIndex(jComboBoxCode.getSelectedIndex());
        	}
         }
         catch(Exception e){
         	System.out.println(e);
         }
//-------------------------------------------------------------------
//为左右按钮加事件--------------------------------------------------
     btright.addActionListener(new ActionListener(){
     	public void actionPerformed(ActionEvent e){
     		try{
	     		if(rs.next()){
	     	       tnumber.setEditable(true);
	               tnumber.setText(""+rs.getInt("R_Number"));
	               tname.setText(rs.getString("E_Number"));
        		   Database.setJComboBox(jComboBoxCode,rs.getString("E_Number"));
        	       //jComboBox.setSelectedItem(jComboBoxCode.getSelectedIndex());
        	       jComboBox.setSelectedIndex(jComboBoxCode.getSelectedIndex());
	            
	               //ttime.setText(rs.getString("R_Date"));
	               ttime.setValue(rs.getDate("R_Date"));
	               tadress.setText(rs.getString("R_Address"));
	               treason.setText(rs.getString("R_Causation"));
	               tremarks.setText(rs.getString("R_Remark"));
	     		}
    			else{
				    new JOptionPane().showMessageDialog(null,"已到尾了！");
				    rs.last();
    			}
	     	 }
     		 catch(Exception el){
     		 	System.out.println(el);
     		 }
     	}
     });

     btleft.addActionListener(new ActionListener(){
     	public void actionPerformed(ActionEvent e){
     		try{
	     		if(rs.previous()){
		     		tnumber.setEditable(true);
		            tnumber.setText(""+rs.getInt("R_Number"));
		            tname.setText(rs.getString("E_Number"));
	        		jComboBoxCode.setSelectedItem(rs.getString("E_Number"));
	        	    jComboBox.setSelectedIndex(jComboBoxCode.getSelectedIndex());
		            
		            //ttime.setText(rs.getString("R_Date"));
		            ttime.setValue(rs.getDate("R_Date"));
		            tadress.setText(rs.getString("R_Address"));
		            treason.setText(rs.getString("R_Causation"));
		            tremarks.setText(rs.getString("R_Remark"));
	     		}
    			else{
			        new JOptionPane().showMessageDialog(null,"已到头了！");
    			}
     		}
     		catch(Exception er){
     		 	System.out.println(er);
     		}
     	}
     });
//-添加添加按扭事件---------------------------------------
     btadd.addActionListener(new ActionListener(){
    	public void actionPerformed(ActionEvent e){
    		tnumber.setText("自动生成");
    		tnumber.setEditable(false);
    		btsave.setEnabled(true);
    		//tname.setText("");
    		ttime.setValue(new Date());
    		treason.setText("");
    		tadress.setText("");
    		tremarks.setText("");
    	}
      });
     // 组合框，选项事件
     jComboBox.addItemListener(new ItemListener() { 
     	public void itemStateChanged(ItemEvent e) { 
    	      jComboBoxCode.setSelectedIndex(jComboBox.getSelectedIndex());
     	} 
     }); 
//-添加保存按扭事件----------------------------------------------------------------
      btsave.addActionListener(new ActionListener(){
        public void actionPerformed(ActionEvent e){
        	if(ttime.getText().equals("") || tadress.getText().equals("") || treason.getText().equals("")){
        	    new JOptionPane().showMessageDialog(null,"姓名,地址,时间,奖励原因!填写不能为空！");
        	}
        	else{        		
				sql="select max(R_Number)+1 from RewardspunishmentInformation";
				int maxnum=1;
        		try {
        			ResultSet rs2 = Database.executeQuery(sql);
        			if(rs2.next()) maxnum=rs2.getInt(1);
        			rs2.close();
        	    }
        	    catch (Exception ex) {
        	    	System.out.println (ex);
        	    }				
        	    jComboBoxCode.setSelectedIndex(jComboBox.getSelectedIndex());
        		
        	String name=tname.getText();
        	String time=ttime.getText();
        	String reason=treason.getText();
        	String address=tadress.getText();
        	String remarks=tremarks.getText();
            ////////////////////////////////////////////////// 下命令需要完善的        	
   	  		String sInsert = "";
			if(Database.dbms.equals("Sql Server")){
				sInsert="insert RewardspunishmentInformation(R_Number,E_Number,R_Date,R_Address,R_Remark,R_Causation) values ("+Integer.toString(maxnum).trim()+","+ jComboBoxCode.getSelectedItem()+",'"+time+"','"+ address +"','"+remarks+"','"+reason+"')";
			}
			if(Database.dbms.equals("Oracle")){
				sInsert="insert into RewardspunishmentInformation(ID,R_Number,E_Number,R_Date,R_Address,R_Remark,R_Causation) values (SRI.nextval,"+Integer.toString(maxnum).trim()+","+ jComboBoxCode.getSelectedItem()+",to_date('"+time+"','YYYY-MM-DD'),'"+ address +"','"+remarks+"','"+reason+"')";
			}
			if(Database.dbms.equals("MySQL")){
				sInsert="insert into RewardspunishmentInformation(ID,R_Number,E_Number,R_Date,R_Address,R_Remark,R_Causation) values (null,"+Integer.toString(maxnum).trim()+","+ jComboBoxCode.getSelectedItem()+",'"+time+"','"+ address +"','"+remarks+"','"+reason+"')";
			}
			if(Database.dbms.equals("PostgreSQL")){
				sInsert="insert into RewardspunishmentInformation(ID,R_Number,E_Number,R_Date,R_Address,R_Remark,R_Causation) values (nextval('RewardspunishmentInformation_ID_seq'),"+Integer.toString(maxnum).trim()+","+ jComboBoxCode.getSelectedItem()+",'"+time+"','"+ address +"','"+remarks+"','"+reason+"')";
			}
        	//System.out.println(sInsert);
        	try{
              	if(Database.executeUpdate(sInsert)!=0){
	        	//if(Database.executeSQL(sInsert)){
		        	tnumber.setEditable(true);
		        	btsave.setEnabled(false);
		        	new JOptionPane().showMessageDialog(null,"成功添加数据！");
		        	sql="select R_Number,E_Number,R_Date,R_Address,R_Causation,R_Remark from RewardspunishmentInformation ";
		        	//Database.joinDB();
                	rs= Database.executeQuery(sql);
		        	//Database.query(sql);
		
		        	rs.last();
		        	tnumber.setText(""+rs.getInt("R_Number"));
	            }
            }catch(Exception el){
             	new JOptionPane().showMessageDialog(null,"添加数据不成功！");
            }
       	    btsave.setEnabled(false);
         }
       }
    });
//---添加修改和删除事件按扭-------------------------------------------------------
   	btdelet.addActionListener(new ActionListener(){
   		public void actionPerformed(ActionEvent e){
   			try{
   				sql="delete from RewardspunishmentInformation where R_Number ='"+ tnumber.getText()+"'";
   				//System.out.println(sql);
   		        if(Database.executeUpdate(sql)!=0){
   				//if(Database.executeSQL(sql)){
   					new JOptionPane().showMessageDialog(null,"数据删除成功！");
   					//Database.joinDB();
   	  				String sqll="select R_Number,E_Number,R_Date,R_Address,R_Causation,R_Remark from RewardspunishmentInformation";
   	         	    rs= Database.executeQuery(sqll);
   	  				//Database.query(sqll);
   	  				rs.last();
                    tnumber.setText(""+rs.getInt("R_Number"));
                    tname.setText(rs.getString("E_Number"));
	        		jComboBoxCode.setSelectedItem(rs.getString("E_Number"));
	        	    jComboBox.setSelectedIndex(jComboBoxCode.getSelectedIndex());
                    
                    //ttime.setText(rs.getString("R_Date"));
                    ttime.setValue(rs.getDate("R_Date"));
                    tadress.setText(rs.getString("R_Address"));
                    treason.setText(rs.getString("R_Causation"));
                    tremarks.setText(rs.getString("R_Remark"));
   				}
   			}
   			catch(Exception el){}
   		}
   	});

   	btamend.addActionListener(new ActionListener(){
   		public void actionPerformed(ActionEvent e){
   			try{
   	   	  		String supdate = "";
   				if(Database.dbms.equals("Sql Server")){
   	   				supdate="update RewardspunishmentInformation set E_Number="+ jComboBoxCode.getSelectedItem()+ ",R_Date='"+ ttime.getText() +"',R_Address='"+tadress.getText()+"',R_Causation='"+treason.getText()+"',R_Remark='"+tremarks.getText()+"' where R_Number='"+tnumber.getText()+"'";
   				}
   				if(Database.dbms.equals("Oracle")){
   	   				supdate="update RewardspunishmentInformation set E_Number="+ jComboBoxCode.getSelectedItem()+ ",R_Date=to_date('"+ ttime.getText() +"','YYYY-MM-DD'),R_Address='"+tadress.getText()+"',R_Causation='"+treason.getText()+"',R_Remark='"+tremarks.getText()+"' where R_Number='"+tnumber.getText()+"'";
   				}
   				if(Database.dbms.equals("MySQL")){
   	   				supdate="update RewardspunishmentInformation set E_Number="+ jComboBoxCode.getSelectedItem()+ ",R_Date='"+ ttime.getText() +"',R_Address='"+tadress.getText()+"',R_Causation='"+treason.getText()+"',R_Remark='"+tremarks.getText()+"' where R_Number='"+tnumber.getText()+"'";
   				}
   				if(Database.dbms.equals("PostgreSQL")){
   	   				supdate="update RewardspunishmentInformation set E_Number="+ jComboBoxCode.getSelectedItem()+ ",R_Date='"+ ttime.getText() +"',R_Address='"+tadress.getText()+"',R_Causation='"+treason.getText()+"',R_Remark='"+tremarks.getText()+"' where R_Number='"+tnumber.getText()+"'";
   				}
   	 			if(Database.executeUpdate(supdate)!=0){
   				//if(Database.executeSQL(supdate)){
   					new JOptionPane().showMessageDialog(null,"数据修改成功！");
   					//Database.joinDB();
   	  				String sqll="select R_Number,E_Number,R_Date,R_Address,R_Causation,R_Remark from RewardspunishmentInformation";
   		            rs= Database.executeQuery(sqll);
   	  				//Database.query(sqll);
   	  				rs.last();
                    tnumber.setText(""+rs.getInt("R_Number"));
                    tname.setText(rs.getString("E_Number"));
	        		jComboBoxCode.setSelectedItem(rs.getString("E_Number"));
	        	    jComboBox.setSelectedIndex(jComboBoxCode.getSelectedIndex());
                     
                    //ttime.setText(rs.getString("R_Date"));
                    ttime.setValue(rs.getDate("R_Date"));
                    tadress.setText(rs.getString("R_Address"));
                    treason.setText(rs.getString("R_Causation"));
                    tremarks.setText(rs.getString("R_Remark"));
   				}
   			}
   			catch(Exception es){
   				new JOptionPane().showMessageDialog(null,"修改数据不成功！");
   			}
   		}
   	});
//-----------------------------------------------------------------
    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    setBounds((screenSize.width-403)/2, (screenSize.height-329)/2, 403, 329);
    this.setClosable(true);
    setVisible(true);
  }

  JButton btadd;
  JButton btamend;
  JButton btdelet;
  JButton btleft;
  JButton btright;
  JButton btsave;
  JLabel lb1;
  JLabel lb2;
  JLabel lb3;
  JLabel lb4;
  JLabel lb5;
  JLabel lb6;
  JLabel lb7;
  JTextField tadress;
  JTextField tname;
  JTextField tnumber;
  JTextField treason;
  JTextArea tremarks;
  //JTextField ttime;
  DateFormat format =new SimpleDateFormat("yyyy-MM-dd"); 
  DateFormatter df = new DateFormatter(format);
  private JFormattedTextField ttime = new JFormattedTextField(df);
}