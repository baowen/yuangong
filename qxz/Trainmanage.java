//培训管理类
package qxz;

import java.awt.*;

import javax.swing.*;

import java.awt.event.*;
import java.sql.*;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

public class Trainmanage extends JInternalFrame {	
	private JButton btadd,btamend,btdelet,btleft,btright,btsave;
    private JLabel lb1,lb2,lb3,lb4,lb5;
    private JTextField tcontent,tdate,tmoney,tnumber,tname;
    private JComboBox jComboBox,jComboBoxCode;

    public Trainmanage(){
        initComponents();
    }

    private void initComponents(){
    	setTitle("培训信息管理");
    	Font f = new Font("新宋体", 0, 14);
    	
    	//定义与初始化组合框
    	jComboBox = new JComboBox();
    	//jComboBox.addItem("男");jComboBox.addItem("女");jComboBox.addItem("");
    	jComboBox.setBackground(new Color(204, 204, 204));
    	jComboBox.setPreferredSize(new Dimension(100, 20));

    	jComboBoxCode = new JComboBox();
    	jComboBoxCode.setBackground(new Color(204, 204, 204));
    	jComboBoxCode.setPreferredSize(new Dimension(100, 20));

        lb1 = new JLabel("员工培训信息管理");
        lb2 = new JLabel("培训编号:");
        lb3 = new JLabel("培训内容:");
        lb4 = new JLabel("培训天数:");
        lb5 = new JLabel("培训费用:");
        JLabel lb6 = new JLabel("员工姓名：");
        tnumber = new JTextField();
        tcontent = new JTextField();
        tmoney = new JTextField();
        tdate = new JTextField();
        tname = new JTextField();
        btright = new JButton(">>");
        btleft = new JButton("<<");
        btdelet = new JButton("删除");
        btamend = new JButton("修改");
        btsave = new JButton("保存");
        btsave.setEnabled(false);
        btadd = new JButton("添加");

        getContentPane().setLayout(null);

        lb1.setFont(new java.awt.Font("新宋体", 1, 18));
        lb1.setForeground(new Color(0, 51, 255));

        getContentPane().add(lb1);
        lb1.setBounds(110, 10, 160, 30);
        lb6.setFont(f);
        getContentPane().add(lb6);
        lb6.setBounds(30,40,70,20);
        lb2.setFont(f);
        getContentPane().add(lb2);
        lb2.setBounds(30, 80, 70, 20);
        lb3.setFont(f);
        getContentPane().add(lb3);
        lb3.setBounds(30, 200, 70, 20);
        lb4.setFont(f);
        getContentPane().add(lb4);
        lb4.setBounds(30, 120, 70, 20);
        lb5.setFont(f);
        getContentPane().add(lb5);
        lb5.setBounds(30, 160, 70, 20);

//        getContentPane().add(tname);
//        tname.setBounds(120,40,110,23);         
        // 添加到Panel上
        getContentPane().add(jComboBox);
        jComboBox.setBounds(120,40,110,23);
        getContentPane().add(jComboBoxCode);
        jComboBoxCode.setBounds(240,40,110,23);
        jComboBoxCode.setVisible(false);
        
        tnumber.setEnabled(false);
        getContentPane().add(tnumber);
        tnumber.setBounds(120, 80, 110, 23);
        getContentPane().add(tcontent);
        tcontent.setBounds(120, 200, 170, 23);
        getContentPane().add(tmoney);
        tmoney.setBounds(120, 160, 140, 23);
        getContentPane().add(tdate);
        tdate.setBounds(120, 120, 110, 23);
        btright.setFont(f);
        getContentPane().add(btright);
        btright.setBounds(330, 255, 50, 20);
        btleft.setFont(f);
        getContentPane().add(btleft);
        btleft.setBounds(330, 235, 50, 20);
        btdelet.setFont(f);
        getContentPane().add(btdelet);
        btdelet.setBounds(250, 245, 70, 25);
        btamend.setFont(f);
        getContentPane().add(btamend);
        btamend.setBounds(170, 245, 70, 25);
        btsave.setFont(f);
        getContentPane().add(btsave);
        btsave.setBounds(10, 245, 70, 25);
        btadd.setFont(f);
        getContentPane().add(btadd);
        btadd.setBounds(90, 245, 70, 25);
        //连接数据库
        //database.joinDB();
        //初始化窗体数据-----------------------------------------------------------------------------
//        String sql="select * from TrainInformation";
        String sql="select E_Number,T_Number,T_Content,T_Money,T_Date from TrainInformation";
        try{
        	if(Database.query(sql)){
        		Database.rs.next();
        		tname.setText(Database.rs.getString("E_Number"));
        		tnumber.setText(Database.rs.getString("T_Number"));
                tcontent.setText(Database.rs.getString("T_Content"));
                tmoney.setText(Database.rs.getString("T_Money"));
                tdate.setText(Database.rs.getString("T_Date"));

                // 更新jComboBox,jComboBoxCode
        		jComboBox.removeAllItems();
        		try {
        			ResultSet rs2 = Database.executeQuery ("select E_Name,E_Number from EmployeeInformation order by E_Name");
        			int row = Database.recCount (rs2);
        			//从结果集中取出Item加入JComboBox中
        			if(row != 0) rs2.beforeFirst ();
        			for (int i = 0; i < row; i++) {
        				rs2.next();
        				jComboBox.addItem (rs2.getString (1));
        				jComboBoxCode.addItem (rs2.getString (2));
        		    }
        			jComboBox.addItem("");
        			jComboBoxCode.addItem("");
        			rs2.close();
        	    }
        	    catch (Exception ex) {
        	    	System.out.println ("initJComboBox (): false");
        	    }      
        	    jComboBoxCode.setSelectedItem(tname.getText());
        	    jComboBox.setSelectedIndex(jComboBoxCode.getSelectedIndex());
        	}
        }
        catch(Exception e){
	    	System.out.println ("1111");
        	System.out.println(e);
        }
       //----------------------------------------------------------------------------------------

        //为上一条下一条按钮加事件-----------------------------------------------------------------
        btright.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent e){
        		try{
        			if(Database.rs.next()){
		        		tname.setText(Database.rs.getString("E_Number"));
		        		
		        		Database.setJComboBox(jComboBoxCode,Database.rs.getString("E_Number"));
		        	    //jComboBox.setSelectedItem(jComboBoxCode.getSelectedIndex());
		        	    jComboBox.setSelectedIndex(jComboBoxCode.getSelectedIndex());
        	    
		        		//jComboBox.setSelectedItem(Database.rs.getString("T_Name"));
		                //jcb1.getSelectedItem().equals("")
		                //jComboBox.getItemCount();
		                //jComboBox.getSelectedItem();
		                //jComboBox.getSelectedIndex();
		                //jComboBox.setSelectedIndex(anIndex);
		                //jComboBox.setSelectedItem(anObject);		                
		        		
		        		tnumber.setText(Integer.toString(Database.rs.getInt("T_Number")));
		                tcontent.setText(Database.rs.getString("T_Content"));
		                tmoney.setText(Database.rs.getString("T_Money"));
		                tdate.setText(Database.rs.getString("T_Date"));              
        			}
        			else{
    				    new JOptionPane().showMessageDialog(null,"已到尾了！");
    				    Database.rs.last();
        			}
        		}
        		catch(Exception e8){
        	    	System.out.println ("22222222");
             	    System.out.println(e8);
			    }
        }});
        btleft.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent e){
        		try{
        			if(Database.rs.previous()){
		        		tname.setText(Database.rs.getString("E_Number"));
		        		jComboBoxCode.setSelectedItem(Database.rs.getString("E_Number"));
		        	    jComboBox.setSelectedIndex(jComboBoxCode.getSelectedIndex());
		        	    
		        		tnumber.setText(Integer.toString(Database.rs.getInt("T_Number")));
		                tcontent.setText(Database.rs.getString("T_Content"));
		                tmoney.setText(Database.rs.getString("T_Money"));
		                tdate.setText(Database.rs.getString("T_Date"));
        			}
        			else{
    			        new JOptionPane().showMessageDialog(null,"已到头了！");
        			}
        		}
        		catch(Exception el){
        	    	System.out.println ("333333333333333");
          	        System.out.println(el);
			    }
        }});
       //------------------------------------------------------------------
       //为添加保存按钮加事件------------------------------------------------------------
        btadd.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent e){
        		btsave.setEnabled(true);
        		tnumber.setText("自动生成");
		        tcontent.setText("");
		        tmoney.setText("");
		        tdate.setText("");
		        //tname.setText("");
               }
        });
        // 组合框，选项事件
        jComboBox.addItemListener(new ItemListener() { 
        	public void itemStateChanged(ItemEvent e) { 
       	      jComboBoxCode.setSelectedIndex(jComboBox.getSelectedIndex());
        	} 
       	}); 

        btsave.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent e){
        		if(tnumber.getText().equals("")||tcontent.getText().equals("")||tmoney.getText().equals("")
        		   ||tdate.getText().equals("")){
        		   	new JOptionPane().showMessageDialog(null,"所有数据均不能为空！");
        		}
        		else{
//					Database.joinDB();
//					String sql="select max(T_Number)+1 from TrainInformation";
//					Database.query(sql);
//					int maxnum=1;
//					try{
//					   //if(Database.rs.getRow()!= 0){ //error
//					   if(Database.rs.next()){						   
//						  maxnum=Database.rs.getInt(1);
//					   }
//					}
//					catch(Exception el){
//						System.out.println(el);
//					}
					String sql="select max(T_Number)+1 from TrainInformation";
					int maxnum=1;
	        		try {
	        			ResultSet rs2 = Database.executeQuery (sql);
	        			if(rs2.next()) maxnum=rs2.getInt(1);
	        			rs2.close();
	        	    }
	        	    catch (Exception ex) {
	        	    	System.out.println (ex);
	        	    }
					
	        	    jComboBoxCode.setSelectedIndex(jComboBox.getSelectedIndex());

	       	  		String sqlInsert = "";
	    			if(Database.dbms.equals("Sql Server")){
	    				sqlInsert="insert TrainInformation(T_Number,T_Content,E_Number,T_Date,T_Money) values("+ Integer.toString(maxnum).trim() +"," + "'"+ tcontent.getText() +"',"+ jComboBoxCode.getSelectedItem() +",'"+ tdate.getText() +"'," + "'"+ tmoney.getText() +"')";
	    			}
	    			if(Database.dbms.equals("Oracle")){
	    				sqlInsert="insert into TrainInformation(ID,T_Number,T_Content,E_Number,T_Date,T_Money) values(STI.nextval,"+ Integer.toString(maxnum).trim() +"," + "'"+ tcontent.getText() +"',"+ jComboBoxCode.getSelectedItem() +",'"+ tdate.getText() +"'," + "'"+ tmoney.getText() +"')";
	    			}
	    			if(Database.dbms.equals("MySQL")){
	    				sqlInsert="insert into TrainInformation(ID,T_Number,T_Content,E_Number,T_Date,T_Money) values(null,"+ Integer.toString(maxnum).trim() +"," + "'"+ tcontent.getText() +"',"+ jComboBoxCode.getSelectedItem() +",'"+ tdate.getText() +"'," + "'"+ tmoney.getText() +"')";
	    			}
	    			if(Database.dbms.equals("PostgreSQL")){
	    				sqlInsert="insert into TrainInformation(ID,T_Number,T_Content,E_Number,T_Date,T_Money) values(nextval('TrainInformation_ID_seq'),"+ Integer.toString(maxnum).trim() +"," + "'"+ tcontent.getText() +"',"+ jComboBoxCode.getSelectedItem() +",'"+ tdate.getText() +"'," + "'"+ tmoney.getText() +"')";
	    			}
	        	    ///String sqlInsert="insert TrainInformation(T_Number,T_Content,T_Name,T_Date,T_Money) values('"+ tnumber.getText() +"'," + "'"+ tcontent.getText() +"','"+ tname.getText() +"','"+ tdate.getText() +"'," + "'"+ tmoney.getText() +"')";
        			//String sqlInsert="insert TrainInformation values('"+ tnumber.getText() +"'," + "'"+ tcontent.getText() +"','"+ tname.getText() +"','"+ tdate.getText() +"'," + "'"+ tmoney.getText() +"')";
        			try{
        				if(Database.executeSQL(sqlInsert)){
        					new JOptionPane().showMessageDialog(null,"数据添加成功！");
        					//database.joinDB();
        					sql="select E_Number,T_Number,T_Content,T_Money,T_Date from TrainInformation";
        					Database.query(sql);
        				}
        			}
        			catch(Exception einsert){}
        			}
            	    btsave.setEnabled(false);
               }
        });
       //-----------------------------------------------------------------------------------
       //为修改删除按钮加事件---------------------------------------------------------------
        btdelet.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent e){
        		String sqldelete="delete from TrainInformation where T_Number='"+ tnumber.getText() +"'";
                try{
                	if(Database.executeSQL(sqldelete)){
                		new JOptionPane().showMessageDialog(null,"数据删除成功！");
    					//database.joinDB();
    					String sql="select E_Number,T_Number,T_Content,T_Money,T_Date from TrainInformation";
    					Database.query(sql);
    					Database.rs.first();
		        		tname.setText(Database.rs.getString("E_Number"));
		        		//jComboBox.setSelectedItem(Database.rs.getString("E_Number"));
		        		jComboBoxCode.setSelectedItem(Database.rs.getString("E_Number"));
		        	    jComboBox.setSelectedIndex(jComboBoxCode.getSelectedIndex());
		        		
		        		tnumber.setText(Database.rs.getString("T_Number"));
		                tcontent.setText(Database.rs.getString("T_Content"));
		                tmoney.setText(Database.rs.getString("T_Money"));
		                tdate.setText(Database.rs.getString("T_Date"));
                		}
                	}
                catch(Exception edelete){}
               }
        });
        btamend.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent e){
        	   String sqlupdate="update TrainInformation set T_Money='"+ tmoney.getText() +"'," +
        	                    "T_Content='"+ tcontent.getText() +"',E_Number="+jComboBoxCode.getSelectedItem() +","+
        	                    "T_Date='"+ tdate.getText() +"' where T_Number='"+ tnumber.getText()+"'";
                try{
                	if(Database.executeSQL(sqlupdate)){
                		new JOptionPane().showMessageDialog(null,"数据修改成功！");
    					//database.joinDB();
    					String sql="select E_Number,T_Number,T_Content,T_Money,T_Date from TrainInformation";
    					Database.query(sql);
                		}
                	}
                catch(Exception edelete){}
                }
        });
       //-----------------------------------------------------------------------------------
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setBounds((screenSize.width-403)/2, (screenSize.height-329)/2, 403, 329);
        this.setClosable(true);
        setVisible(true);
    }
}
