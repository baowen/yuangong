package qxz;
public class chStr {
/***************************************************
   *method name:    chStr()
   *method function:解决输出中文乱码问题
   *return value:  String
   *2007-7-05
****************************************************/
  public static String chStrgbk(String str){
	if(str==null){
		str="";
	}else{
		try{
  		   str=(new String(str.getBytes("iso-8859-1"),"gbk")).trim();
	           str=str.replaceAll("'","''");
		}catch(Exception e){
		   e.printStackTrace(System.err);
		}
	}
	return str;
  }
  public static String chStr(String str){
	if(str==null){
	   str="";
	}else{
  	   try{
  		  // mysql支持的字符集，latin1 big5 czech euc_kr gb2312 gbk latin1_de 
  		  // mysql支持的字符集，sjis tis620 ujis dec8 dos german1 hp8 koi8_ru latin2 
  		  // mysql支持的字符集，swe7 usa7 cp1251 danish hebrew win1251 estonia hungarian 
  		  // mysql支持的字符集，koi8_ukr win1251ukr greek win1250 croat cp1257 latin5   		   

  		  //str=(new String(str.getBytes("iso-8859-1"),"gbk")).trim();
	      str=(new String(str.getBytes("iso-8859-1"),"gb2312")).trim();
	      //str=(new String(str.getBytes("iso-8859-1"),"big5")).trim();

  		  //str=(new String(str.getBytes("latin2"),"gbk")).trim();
 	      //str=(new String(str.getBytes("latin2"),"gb2312")).trim();
 	      //str=(new String(str.getBytes("latin2"),"big5")).trim();
 	      
   		  //str=(new String(str.getBytes("latin1"),"gbk")).trim();
  	      //str=(new String(str.getBytes("latin1"),"gb2312")).trim();
  	      //str=(new String(str.getBytes("latin2"),"big5")).trim();

  		  //str=(new String(str.getBytes("hebrew"),"gbk")).trim();
  	      //str=(new String(str.getBytes("hebrew"),"gb2312")).trim();
  	      //str=(new String(str.getBytes("hebrew"),"big5")).trim(); 	     
  		 	      
	      //str=(new String(str.getBytes("utf8"),"gb2312")).trim();
	      //str=(new String(str.getBytes("utf8"),"gbk")).trim();
	      //str=(new String(str.getBytes("utf8"),"big5")).trim();
	      
	      //str=(new String(str.getBytes("gbk"),"gb2312")).trim();// jdbc ok
 	      //str=(new String(str.getBytes("gbk"),"utf8")).trim();  // jdbc no ok
 	      //str=(new String(str.getBytes("gbk"),"big5")).trim(); // jdbc no ok 	      
	      str=str.replaceAll("'","''");
	   }catch(Exception e){
 	      e.printStackTrace(System.err);
	   }
	}
	return str;
  }
  public String chStrgb2312(String str){
	if(str==null){
	   str="";
	}else{
  	   try{
	      str=(new String(str.getBytes("iso-8859-1"),"GB2312")).trim();
	      str=str.replaceAll("'","''");
	   }catch(Exception e){
 	      e.printStackTrace(System.err);
	   }
	}
	return str;
  }
/*  public String chStr(String str){
	if(str==null){
	   str="";
	}else{
  	   try{
	      str=(new String(str.getBytes("iso-8859-1"),"GB2312")).trim();
	      str=str.replaceAll("'","''");
	   }catch(Exception e){
 	      e.printStackTrace(System.err);
	   }
	}
	return str;
  }*/
  public String convertStr(String str1){
	if(str1==null){
		str1="";
	}else{
		try{
			str1=str1.replaceAll(" ","&nbsp;");
			str1=str1.replaceAll("\r\n","<br>");
		}catch(Exception e){
			e.printStackTrace(System.err);
		}
	}
	return str1;
  }
}
