//数据库联接类
package qxz;

import java.sql.*;
import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import java.io.File;
//import com.microsoft.sqlserver.jdbc.*;
//import com.microsoft.jdbc.sqlserver.*;
//import java.io.File;

public class Database {
    public static Connection cn;
    public static Statement st;
    public static Statement st2;
    public static ResultSet rs;
    public static String dbms;
    public static String Linknum="0";
    
	// below for SQL Server
    static String user = ConfigIni.getIniKey ("UserID");
    static String pwd  = ConfigIni.getIniKey ("Password");
    static String ip   = ConfigIni.getIniKey ("IP");
    static String acc  = ConfigIni.getIniKey ("Access");
    static String dbf  = ConfigIni.getIniKey ("DataBase");
	// below for Oracle
    static String UID=ConfigIni.getIniKey ("UID");
    static String Passd=ConfigIni.getIniKey ("Passd");
    static String Server=ConfigIni.getIniKey ("Server");
    static String DB=ConfigIni.getIniKey ("DB");
    static String Port=ConfigIni.getIniKey ("Port");			
	// below for MySQL
    static String UID2=ConfigIni.getIniKey ("UID2");
    static String Passd2=ConfigIni.getIniKey ("Passd2");
    static String Server2=ConfigIni.getIniKey ("Server2");
    static String DB2=ConfigIni.getIniKey ("DB2");
    static String Port2=ConfigIni.getIniKey ("Port2");			
	// below for PostgreSQL
    static String UID3=ConfigIni.getIniKey ("UID3");
    static String Passd3=ConfigIni.getIniKey ("Passd3");
    static String Server3=ConfigIni.getIniKey ("Server3");
    static String DB3=ConfigIni.getIniKey ("DB3");
    static String Port3=ConfigIni.getIniKey ("Port3");			

    static {
		try {		
//			String user = ConfigIni.getIniKey ("UserID");
//			String pwd  = ConfigIni.getIniKey ("Password");
//			String ip   = ConfigIni.getIniKey ("IP");
//			String acc  = ConfigIni.getIniKey ("Access");
			//String dbf  = ConfigIni.getIniKey ("DataBase");
			// below for Oracle
			//String UID=ConfigIni.getIniKey ("UID");
			//String Passd=ConfigIni.getIniKey ("Passd");
			//String Server=ConfigIni.getIniKey ("Server");
			//String DB=ConfigIni.getIniKey ("DB");
			//String Port=ConfigIni.getIniKey ("Port");			
			// below for MySQL
			//String UID2=ConfigIni.getIniKey ("UID2");
			//String Passd2=ConfigIni.getIniKey ("Passd2");
			//String Server2=ConfigIni.getIniKey ("Server2");
			//String DB2=ConfigIni.getIniKey ("DB2");
			//String Port2=ConfigIni.getIniKey ("Port2");			
			// below for PostgreSQL
			//String UID3=ConfigIni.getIniKey ("UID3");
			//String Passd3=ConfigIni.getIniKey ("Passd3");
			//String Server3=ConfigIni.getIniKey ("Server3");
			//String DB3=ConfigIni.getIniKey ("DB3");
			//String Port3=ConfigIni.getIniKey ("Port3");			
			
			if(ConfigIni.getIniKey ("Default_Link").equals ("1")) {		//JDBC连接方式--SQL Server 2005
				////////////////////////////////////////////////////////////////////////////////////////////
				////Microsoft SQLServer(http://jtds.sourceforge.net)  // 另外一种驱动OK 
				//Class.forName( "net.sourceforge.jtds.jdbc.Driver" ); 
				//cn = DriverManager.getConnection( "jdbc:jtds:sqlserver://"+ip+":1433/"+dbf, user, pwd);
				////////////////////////////////////////////////////////////////////////////////////////////
				//注册驱动
				//DriverManager.registerDriver (new com.microsoft.jdbc.sqlserver.SQLServerDriver()); //2000
				DriverManager.registerDriver (new com.microsoft.sqlserver.jdbc.SQLServerDriver());  //2005
				//获得一个连接
				//String url  = "jdbc:microsoft:sqlserver://" + ip + ":" + acc + ";" + "databasename=" + dbf;//2000
				String url  = "jdbc:sqlserver://" + ip + ":" + acc + ";" + "databasename=" + dbf;  //2005
				cn = DriverManager.getConnection (url, user, pwd);
				
                //// 使用连接池 http://www.slink.nildram.co.uk/java/DBPool/index.html
				//snaq.db.ConnectionPool pool = new snaq.db.ConnectionPool("local",10,12,180000,url,user, pwd);
				//Connection con = null;
				//long timeout = 2000;  // 2 second timeout
				//try
				//{ //cn = pool.getConnection(timeout);
				//  Connection cn2 = pool.getConnection(timeout);
				//  Connection cn3 = pool.getConnection(timeout);
				//  Connection cn4 = pool.getConnection(timeout);
				//  Connection cn5 = pool.getConnection(timeout);
				//  Connection cn6 = pool.getConnection(timeout);
				//  Connection cn7 = pool.getConnection(timeout);
				//  Connection cn8 = pool.getConnection(timeout);
				//  Connection cn9 = pool.getConnection(timeout);
				//  Connection cn10 = pool.getConnection(timeout);
				//  Connection cn11 = pool.getConnection(timeout);
				//  Connection cn12 = pool.getConnection(timeout);
				//  Connection cn13 = pool.getConnection(timeout);
				//  cn=cn13;
				//  //if (cn != null)  //  //...use the connection...
				//  //else //  //...do something else (timeout occurred)...
				//}
				//catch (SQLException sqle){ //...deal with exception...}					
				//}
				//finally
				//{ //try { con.close(); } //catch (SQLException e) { ... }}
				//}
				
				dbms="Sql Server";				
			}
			else if(ConfigIni.getIniKey ("Default_Link").equals ("2")) {	//JDBC连接方式--SQL Server 2000
				//注册驱动
				DriverManager.registerDriver (new com.microsoft.jdbc.sqlserver.SQLServerDriver()); //2000
				//DriverManager.registerDriver (new com.microsoft.sqlserver.jdbc.SQLServerDriver());  //2005
				//获得一个连接
				String url  = "jdbc:microsoft:sqlserver://" + ip + ":" + acc + ";" + "databasename=" + dbf;//2000
				//String url  = "jdbc:sqlserver://" + ip + ":" + acc + ";" + "databasename=" + dbf;  //2005
				cn = DriverManager.getConnection (url, user, pwd);
				dbms="Sql Server";
			}
			else if(ConfigIni.getIniKey ("Default_Link").equals ("4")) {//JDBC-ODBC连接方式 to Oracle Database
				//注册驱动										//JDBC-ODBC连接方式
				DriverManager.registerDriver(new sun.jdbc.odbc.JdbcOdbcDriver());
				//获得一个连接
				cn = DriverManager.getConnection ("jdbc:odbc:"+ConfigIni.getIniKey("LinkNameORA").trim(),UID,Passd);
				// + ConfigIni.getIniKey("LinkName")
				dbms="Oracle";
			}
			else if(ConfigIni.getIniKey ("Default_Link").equals ("5")) {		//JDBC连接方式 to Oracle Database
 			   DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
		       String url = "jdbc:oracle:thin:@"+Server+":"+Port+":"+DB;
 			   //Connection conn=DriverManager.getConnection("jdbc:oracle:thin:@rich:1521:ora9201","scott",tiger");
 		       cn =DriverManager.getConnection (url, UID, Passd);
               //System.out.println (url);
				dbms="Oracle";
			}
			else if(ConfigIni.getIniKey ("Default_Link").equals ("6")) {		//JDBC连接方式 to MySQL Database
		        try {
		            // The newInstance() call is a work around for some
		            // broken Java implementations
		            Class.forName("com.mysql.jdbc.Driver").newInstance();
		        } catch (Exception ex) {
		            // handle the error
		        }
		        String url = "jdbc:mysql://localhost/"+DB2+"?"+"user="+UID2+"&"+"password="+Passd2;
		        try {
		            cn=DriverManager.getConnection(url);
		            // Do something with the Connection
		        } catch (SQLException ex) {
		            // handle any errors
		            System.out.println("SQLException: " + ex.getMessage());
		            System.out.println("SQLState: " + ex.getSQLState());
		            System.out.println("VendorError: " + ex.getErrorCode());
		        }
				dbms="MySQL";
			}
			else if(ConfigIni.getIniKey ("Default_Link").equals ("7")) {//JDBC-ODBC连接方式 to MySQL Database
				//注册驱动										//JDBC-ODBC连接方式
				DriverManager.registerDriver(new sun.jdbc.odbc.JdbcOdbcDriver());
				//获得一个连接
				cn = DriverManager.getConnection ("jdbc:odbc:"+ConfigIni.getIniKey("LinkNameMySQL").trim(),UID2,Passd2);
				dbms="MySQL";
				Linknum="7";
			}			
			else if(ConfigIni.getIniKey ("Default_Link").equals ("8")) {		//JDBC-ODBC连接方式 to Oracle Database
				//JDBC TO postgresql
				Class.forName("org.postgresql.Driver"); 
			    String url = "jdbc:postgresql://"+Server3+":"+Port3+"/"+DB3; 
			    cn = DriverManager.getConnection(url, UID3, Passd3); 			
				dbms="PostgreSQL";
			}
			else if(ConfigIni.getIniKey ("Default_Link").equals ("9")) {//JDBC-ODBC连接方式 to PostgreSQL Database
				//注册驱动										//JDBC-ODBC连接方式
				DriverManager.registerDriver(new sun.jdbc.odbc.JdbcOdbcDriver());
				//获得一个连接
				cn = DriverManager.getConnection ("jdbc:odbc:"+ConfigIni.getIniKey("LinkNamePostgreSQL").trim(),UID3,Passd3);
				dbms="PostgreSQL";
			}
			else if(ConfigIni.getIniKey ("Default_Link").equals ("10")) {//JDBC-ODBC连接方式 to access Database
				//注册驱动										//JDBC-ODBC连接方式
				DriverManager.registerDriver(new sun.jdbc.odbc.JdbcOdbcDriver()); //Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");  // also ok
				//获得一个连接
				//String url="jdbc:odbc:Driver={MicroSoft Access Driver (*.mdb)};DBQ="+new File("sqlserver20002005jdbc").getAbsolutePath()+"\\EmployeeIMS.mdb";
				String url="jdbc:odbc:Driver={MicroSoft Access Driver (*.mdb)};DBQ=sqlserver20002005jdbc\\EmployeeIMS.mdb";
				System.out.println(url);
				cn = DriverManager.getConnection (url,"","");
				dbms="Access";
			}
			else {	//ConfigIni.getIniKey ("Default_Link").equals ("3") //JDBC-ODBC连接方式 to SQL Server Database
				//注册驱动										//JDBC-ODBC连接方式
				DriverManager.registerDriver(new sun.jdbc.odbc.JdbcOdbcDriver());
				//获得一个连接
				//conn = DriverManager.getConnection ("jdbc:odbc:sam",user,pwd);
				cn = DriverManager.getConnection ("jdbc:odbc:"+ConfigIni.getIniKey("LinkName").trim(),user,pwd);
				// + ConfigIni.getIniKey("LinkName")
				dbms="Sql Server";
			}
		
			//设置自动提交为false
			//cn.setAutoCommit(false);  //要有专门的递交命令 cn.commit();
			//建立高级载体
			//st = cn.createStatement (ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            st = cn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
            st2 = cn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
	    }
	    catch (Exception ex) {
	    	System.out.println(ex.getMessage().toString()+"--");
	    	JOptionPane.showMessageDialog (null, "数据库连接失败...", "错误", JOptionPane.ERROR_MESSAGE);
	    	System.exit(0);
	    	//ex.printStackTrace();
	    }//End try
    }
    
    public static boolean joinDB() {
        boolean joinFlag;
		try {		
			joinFlag = true;
			//String user = ConfigIni.getIniKey ("UserID");
			//String pwd  = ConfigIni.getIniKey ("Password");
			//String ip   = ConfigIni.getIniKey ("IP");
			//String acc  = ConfigIni.getIniKey ("Access");
			//String dbf  = ConfigIni.getIniKey ("DataBase");
			// below for Oracle
			//String UID=ConfigIni.getIniKey ("UID");
			//String Passd=ConfigIni.getIniKey ("Passd");
			//String Server=ConfigIni.getIniKey ("Server");
			//String DB=ConfigIni.getIniKey ("DB");
			//String Port=ConfigIni.getIniKey ("Port");			
			// below for MySQL
			//String UID2=ConfigIni.getIniKey ("UID2");
			//String Passd2=ConfigIni.getIniKey ("Passd2");
			//String Server2=ConfigIni.getIniKey ("Server2");
			//String DB2=ConfigIni.getIniKey ("DB2");
			//String Port2=ConfigIni.getIniKey ("Port2");			
			// below for PostgreSQL
			//String UID3=ConfigIni.getIniKey ("UID3");
			//String Passd3=ConfigIni.getIniKey ("Passd3");
			//String Server3=ConfigIni.getIniKey ("Server3");
			//String DB3=ConfigIni.getIniKey ("DB3");
			//String Port3=ConfigIni.getIniKey ("Port3");			
			
			if(ConfigIni.getIniKey ("Default_Link").equals ("1")) {		//JDBC连接方式
				////////////////////////////////////////////////////////////////////////////////////////////
				////Microsoft SQLServer(http://jtds.sourceforge.net)  // 另外一种驱动OK 
				//Class.forName( "net.sourceforge.jtds.jdbc.Driver" ); 
				//cn = DriverManager.getConnection( "jdbc:jtds:sqlserver://"+ip+":1433/"+dbf, user, pwd);
				////////////////////////////////////////////////////////////////////////////////////////////
				//注册驱动
				//DriverManager.registerDriver (new com.microsoft.jdbc.sqlserver.SQLServerDriver()); //2000
				DriverManager.registerDriver (new com.microsoft.sqlserver.jdbc.SQLServerDriver());  //2005
				//获得一个连接
				//String url  = "jdbc:microsoft:sqlserver://" + ip + ":" + acc + ";" + "databasename=" + dbf;//2000
				String url  = "jdbc:sqlserver://" + ip + ":" + acc + ";" + "databasename=" + dbf;  //2005
				cn = DriverManager.getConnection (url, user, pwd);
				
                //// 使用连接池 http://www.slink.nildram.co.uk/java/DBPool/index.html
				//snaq.db.ConnectionPool pool = new snaq.db.ConnectionPool("local",10,12,180000,url,user, pwd);
				//Connection con = null;
				//long timeout = 2000;  // 2 second timeout
				//try
				//{ //cn = pool.getConnection(timeout);
				//  Connection cn2 = pool.getConnection(timeout);
				//  Connection cn3 = pool.getConnection(timeout);
				//  Connection cn4 = pool.getConnection(timeout);
				//  Connection cn5 = pool.getConnection(timeout);
				//  Connection cn6 = pool.getConnection(timeout);
				//  Connection cn7 = pool.getConnection(timeout);
				//  Connection cn8 = pool.getConnection(timeout);
				//  Connection cn9 = pool.getConnection(timeout);
				//  Connection cn10 = pool.getConnection(timeout);
				//  Connection cn11 = pool.getConnection(timeout);
				//  Connection cn12 = pool.getConnection(timeout);
				//  Connection cn13 = pool.getConnection(timeout);
				//  cn=cn13;
				//  //if (cn != null)  //  //...use the connection...
				//  //else //  //...do something else (timeout occurred)...
				//}
				//catch (SQLException sqle){ //...deal with exception...}
				//}
				//finally
				//{ //try { con.close(); } //catch (SQLException e) { ... }}
				//}			
			}
			else if(ConfigIni.getIniKey ("Default_Link").equals ("2")) {	//JDBC连接方式
				//注册驱动
				DriverManager.registerDriver (new com.microsoft.jdbc.sqlserver.SQLServerDriver()); //2000
				//DriverManager.registerDriver (new com.microsoft.sqlserver.jdbc.SQLServerDriver());  //2005
				//获得一个连接
				String url  = "jdbc:microsoft:sqlserver://" + ip + ":" + acc + ";" + "databasename=" + dbf;//2000
				//String url  = "jdbc:sqlserver://" + ip + ":" + acc + ";" + "databasename=" + dbf;  //2005
				cn = DriverManager.getConnection (url, user, pwd);
			}
			else if(ConfigIni.getIniKey ("Default_Link").equals ("4")) {//JDBC-ODBC连接方式 to Oracle Database
				//注册驱动										//JDBC-ODBC连接方式
				DriverManager.registerDriver(new sun.jdbc.odbc.JdbcOdbcDriver());
				//获得一个连接
				cn = DriverManager.getConnection ("jdbc:odbc:"+ConfigIni.getIniKey("LinkNameORA").trim(),UID,Passd);
				// + ConfigIni.getIniKey("LinkName")
			}
			else if(ConfigIni.getIniKey ("Default_Link").equals ("5")) {		//JDBC连接方式 to Oracle Database
	 			DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
			    String url = "jdbc:oracle:thin:@"+Server+":"+Port+":"+DB;
	 			//Connection conn=DriverManager.getConnection("jdbc:oracle:thin:@rich:1521:ora9201","scott",tiger");
	 		    cn =DriverManager.getConnection (url, UID, Passd);
	            //System.out.println (url);
			}
			else if(ConfigIni.getIniKey ("Default_Link").equals ("6")) {		//JDBC连接方式 to MySQL Database
		        try {
		            // The newInstance() call is a work around for some
		            // broken Java implementations
		            Class.forName("com.mysql.jdbc.Driver").newInstance();
		        } catch (Exception ex) {
		            // handle the error
		        }
		        String url = "jdbc:mysql://localhost/"+DB2+"?"+"user="+UID2+"&"+"password="+Passd2;
		        try {
		            cn=DriverManager.getConnection(url);
		            // Do something with the Connection
		        } catch (SQLException ex) {
		            // handle any errors
		            System.out.println("SQLException: " + ex.getMessage());
		            System.out.println("SQLState: " + ex.getSQLState());
		            System.out.println("VendorError: " + ex.getErrorCode());
		        }
			}
			else if(ConfigIni.getIniKey ("Default_Link").equals ("7")) {//JDBC-ODBC连接方式 to MySQL Database
				//注册驱动										//JDBC-ODBC连接方式
				DriverManager.registerDriver(new sun.jdbc.odbc.JdbcOdbcDriver());
				//获得一个连接
				cn = DriverManager.getConnection ("jdbc:odbc:"+ConfigIni.getIniKey("LinkNameMySQL").trim(),UID2,Passd2);
			}			
			else if(ConfigIni.getIniKey ("Default_Link").equals ("8")) {		//JDBC-ODBC连接方式 to Oracle Database
				//JDBC TO postgresql
				Class.forName("org.postgresql.Driver"); 
			    String url = "jdbc:postgresql://"+Server3+":"+Port3+"/"+DB3; 
			    cn = DriverManager.getConnection(url, UID3, Passd3); 			
			}
			else if(ConfigIni.getIniKey ("Default_Link").equals ("9")) {//JDBC-ODBC连接方式 to PostgreSQL Database
				//注册驱动										//JDBC-ODBC连接方式
				DriverManager.registerDriver(new sun.jdbc.odbc.JdbcOdbcDriver());
				//获得一个连接
				cn = DriverManager.getConnection ("jdbc:odbc:"+ConfigIni.getIniKey("LinkNamePostgreSQL").trim(),UID3,Passd3);
			}
			else if(ConfigIni.getIniKey ("Default_Link").equals ("10")) {//JDBC-ODBC连接方式 to access Database
				//注册驱动										//JDBC-ODBC连接方式
				DriverManager.registerDriver(new sun.jdbc.odbc.JdbcOdbcDriver()); //Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");  // also ok
				//获得一个连接
				//String url="jdbc:odbc:Driver={MicroSoft Access Driver (*.mdb)};DBQ="+new File("sqlserver20002005jdbc").getAbsolutePath()+"\\EmployeeIMS.mdb";
				String url="jdbc:odbc:Driver={MicroSoft Access Driver (*.mdb)};DBQ=sqlserver20002005jdbc\\EmployeeIMS.mdb";
				System.out.println(url);
				cn = DriverManager.getConnection (url,"","");
				dbms="Access";
			}
			else {	//ConfigIni.getIniKey ("Default_Link").equals ("3") //JDBC-ODBC连接方式 to SQL Server Database
				//注册驱动										//JDBC-ODBC连接方式
				DriverManager.registerDriver(new sun.jdbc.odbc.JdbcOdbcDriver());
				//获得一个连接
				//conn = DriverManager.getConnection ("jdbc:odbc:sam",user,pwd);
				cn = DriverManager.getConnection ("jdbc:odbc:"+ConfigIni.getIniKey("LinkName").trim(),user,pwd);
				// + ConfigIni.getIniKey("LinkName")
			}
			
			//设置自动提交为false
			//cn.setAutoCommit(false);  //要有专门的递交命令 cn.commit();
			//建立高级载体
			//st = cn.createStatement (ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            st = cn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
            st2 = cn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
	    }
	    catch (Exception ex) {
	    	System.out.println(ex.getMessage().toString()+"--");
	    	JOptionPane.showMessageDialog (null, "数据库连接失败....."+ex.getMessage().toString(), "错误", JOptionPane.ERROR_MESSAGE);
	    	System.exit(0);
	    	joinFlag = false;
	    	//ex.printStackTrace();
	    }//End try
        return joinFlag;    
//        // OLD
//        try {
//            joinFlag = true;
//            Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
//            cn = DriverManager.getConnection("jdbc:odbc:EmployeeIMS","sa","sasasasa");
//            cn.setCatalog("EmployeeIMS");
//            System.out.println("数据库连接成功");
//            st = cn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
//            st2 = cn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
//            return joinFlag;
//        } catch (SQLException sqlEx) {
//            System.out.println(sqlEx.getMessage());
//            joinFlag = false;
//            return joinFlag;
//        } catch (ClassNotFoundException notfoundEX) {
//            System.out.println(notfoundEX.getMessage());
//            joinFlag = false;
//            return joinFlag;
//        }
    }

    public static boolean executeSQL(String sqlString) {
        boolean executeFlag;
        try {
            st.execute(sqlString);
            executeFlag = true;
        } catch (Exception e) {
            executeFlag = false;
            System.out.println("sql exception:" + e.getMessage());
        }
        return executeFlag;
    }

    public static boolean query(String sqlString) {
        try {
            rs = null;
            //System.out.println(sqlString);
            rs = st.executeQuery(sqlString);
        } catch (Exception Ex) {
            System.out.println("sql exception:" + Ex);
            return false;
        }
        return true;
    }
    
	public static int recCount(ResultSet rrs) {
		int i = 0;
		try {
			if(rrs.getRow() != 0)
				rrs.beforeFirst();
			//while用于计算rrs的记录条数
			while(rrs.next())
				i++;
			rrs.beforeFirst();	
	    }
		catch(Exception ex) {
	    	ex.printStackTrace();
	    }//End try
		return i;
	}
	// use another statement st2
	public static int executeUpdate(String sql) {
		int i = 0 ;
		try {
            st2 = cn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY); // 需要此句否则相互干扰
			i = st2.executeUpdate(sql) ;
			cn.commit();
		}
		catch(Exception e) {
			e.printStackTrace() ;
		}//End try
		return i ;
	}
	// use another statement st2
	public static ResultSet executeQuery(String sql) {
		ResultSet rs = null ;
		try {
            st2 = cn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);// 需要此句否则相互干扰
			rs = st2.executeQuery(sql) ;			
		}catch(Exception e) {
			e.printStackTrace() ;
		}//End try
		return rs ;
	}	
	
	/**=======================================================================**
	 *		[## public static void initJComboBox (JComboBox cb, String sqlCode) {} ] :
	 *			参数   ：JComboBox表示要加数据的下拉框, String对象表示一条SQL语句
	 *			返回值 ：JComboBox, 表示返回一个加好Item的JComboBox对象
	 *			修饰符 ：public static 可以不实例化对象而直接调用方法
	 *			功能   ：按SQL语句从数据库选出Items加入JComboBox
	 **=======================================================================**
	 */
	public static void initJComboBox (JComboBox jComboBox, String sqlCode) {
		jComboBox.removeAllItems();
		try {
			ResultSet rs = executeQuery (sqlCode);
			int row = recCount (rs);
			//从结果集中取出Item加入JComboBox中
			if(row != 0) rs.beforeFirst ();
			for (int i = 0; i < row; i++) {
				rs.next();
				jComboBox.addItem (rs.getString (1));
		    }
			jComboBox.addItem("");
	    }
	    catch (Exception ex) {
	    	System.out.println ("sunsql.initJComboBox (): false");
	    }
	}

	public static void setJComboBox (JComboBox jComboBox, String item) {
		try {
			jComboBox.setSelectedItem(item);
	    }
	    catch (Exception ex) {
	    	System.out.println ("setJComboBox (): false");
	    }
	}	

	/**=======================================================================**
	 *		[## public static void initJList (JList jt, String sqlCode) {} ] :
	 *			参数   ：JList表示要加数据的列表框, String对象表示一条SQL语句
	 *			返回值 ：无
	 *			修饰符 ：public static 可以不实例化对象而直接调用方法
	 *			功能   ：按SQL语句从数据库选出数据加入JList
	 **=======================================================================**
	 */
	public static void initJList (JList jt, String sqlCode) {
		try {
			ResultSet rs = executeQuery (sqlCode);
			int row = recCount (rs);
			String list[] = new String[row];
			//从结果集中取出数据存入数组中
			if(row != 0) rs.beforeFirst ();
			for (int i = 0; i < row; i++) {
				rs.next();
				list[i] = rs.getString(1);
		    }//Endfor
		    jt.setListData(list);	//初始化List
	    }
	    catch (Exception ex) {
	    	System.out.println ("sunsql.initJList(): false");
	    }//Endtry
	}
	/**=======================================================================**
	 *		[## public static void initDTM (DefaultTableModel fdtm, String sqlCode) {} ] :
	 *			参数   ：DefaultTableModel对象表示要添充数据的表模式
	 *					 String对象表示一条SQL语句
	 *			返回值 ：无
	 *			修饰符 ：public static 可以不实例化对象而直接调用方法
	 *			功能   ：按SQL语句从数据库中获得数据，添加到fdtm中(也可以说JTable中)
	 **=======================================================================**
	 */
	public static void initDTM (DefaultTableModel fdtm, String sqlCode) {
		try {
			ResultSet rs = executeQuery( sqlCode );	//获得结果集
			int row = recCount( rs );				//获得结果集中有几行数据
			ResultSetMetaData rsm =rs.getMetaData();	//获得列集
			int col = rsm.getColumnCount();		//获得列的个数
			String colName[] = new String[col];
			//取结果集中的表头名称, 放在colName数组中
			for (int i = 0; i < col; i++) {
				colName[i] = rsm.getColumnName( i + 1 );
			}//End for
			if(row != 0) rs.beforeFirst();
			String data[][] = new String[row][col];
			//取结果集中的数据, 放在data数组中
			for (int i = 0; i < row; i++) {
				rs.next();
				for (int j = 0; j < col; j++) {
					data[i][j] = rs.getString (j + 1);
					//System.out.println (data[i][j]);
			    }
			}//End for
			fdtm.setDataVector (data, colName);
	    }
	    catch (Exception ex) {
	    	System.out.println ("SmallSql.initDTM (): false");
	    }//End try
	}
	/**=======================================================================**
	 *		[## public static boolean insert(String sqlString) {} ] :
	 *			参数   ：String对象表示一条SQL语句
	 *			返回值 ：boolean
	 *			修饰符 ：public static 可以不实例化对象而直接调用方法
	 *			功能   ：按SQL语句向数据库中插入数据
	 **=======================================================================**
	 */
	public static boolean insert(String sqlString) {
        boolean insertFlag;
        try {
            st2.executeUpdate(sqlString);
            insertFlag = true;
        } 
        catch (Exception e) {
        	insertFlag = false;
            System.out.println("sql exception:" + e.getMessage());
        }
        return insertFlag;
    }
	private boolean isAlphater(char ch) { 
		if(ch >= 'a' && ch <= 'z' || ch >= 'A' && ch <= 'Z') 
		return true; 
		else 
		return false; 
	} 	
}
